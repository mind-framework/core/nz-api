//Package filter valida y procesa parametros url
package filter

import (
	"errors"
	"log"
	"net/url"
	"regexp"
	// "strings"
	// "errors"
)

// "gitlab.com/mind-framework/core/nz-api/middleware"
// "gitlab.com/mind-framework/core/nz-api/middleware"

const (
	equal          = "="
	greater        = ">>"
	greaterOrEqual = ">="
	less           = "<<"
	lessOrEqual    = "<="
	notEqual       = "<>"
	subString      = "**"
	inList         = "()"
)

var operadoresSQL = map[string]string{
	equal:          " = ",
	greater:        " > ",
	greaterOrEqual: " >= ",
	less:           " < ",
	lessOrEqual:    " <=  ",
	notEqual:       " !=  ",
	subString:      " LIKE  ",
	inList:         " IN ",
}

var sentenciasSQL = map[string]string{
	equal:          " = ?",
	greater:        " > ?",
	greaterOrEqual: " >= ?",
	less:           " < ?",
	lessOrEqual:    " <= ? ",
	notEqual:       " != ? ",
	subString:      " LIKE CONCAT('%',?,'%') ",
	inList:         " IN ( ? )",
}

func init() {

}

//Filters struct para validar y procesar parametros url
type Filters struct {
	AcceptParams []ValidFilter
	Params       []Param
}

//SetParams Setea los parametros en Filters urlValues
func (f *Filters) SetParams(list url.Values) (bool, error) {
	for _, b := range f.AcceptParams {
		log.Printf("Filtro Campo: %#v", b.Key)
		if len(list[b.Key]) == 0 {
			continue
		}

		log.Printf("Filtro Value: %#v", list[b.Key])

		var param Param
		param.Key = b.Field
		param.Type = "CustomSQL"

		multiples := []string{}
		// Valido  todos los valores recibidos para este campo
		for _, value := range list[b.Key] {
			operator := "="
			if len(value) >= 2 {
				operator = value[0:2]
				if _, ok := sentenciasSQL[operator]; ok {
					value = value[2:]
				} else {
					operator = "="
				}
			}
			if !f.ValidateValue(value, b.TypeKey, b.Pattern) {
				// continue
				return false, errors.New("Parametro invalido (" + value + ")")
			}

			if b.CustomSQL != "" {
				param.CustomSQL = b.CustomSQL
				param.Value = value
				f.Params = append(f.Params, param)
			} else if sentencia, ok := sentenciasSQL[operator]; ok {
				log.Printf("[+] Sentencia SQL para operador'%v': '%v'\n", operator, sentencia)

				param.CustomSQL = b.Field + sentenciasSQL[operator]
				param.Operator = operadoresSQL[operator]
				if operator != equal && operator != inList {
					if value == "" {
						param.CustomSQL += " OR " + b.Field + " IS NOT NULL"
						param.Value = ""
					} else {
						param.Value = value
					}
					f.Params = append(f.Params, param)
				} else {
					if value == "" {
						param.CustomSQL += " OR  " + b.Field + " IS NULL"
						param.Value = ""
						f.Params = append(f.Params, param)
					} else {
						multiples = append(multiples, value)
					}
				}
			} else {
				log.Printf("[!] No se encontró Sentencia CustomSQL para operador'%v'\n", operator)
			}

		}

		if len(multiples) > 1 {
			param.CustomSQL = b.Field + sentenciasSQL[inList]
		}

		if len(multiples) > 0 {
			param.Value = multiples
			f.Params = append(f.Params, param)
		}

	}
	return true, nil
}

//ValidateValue Sanitiza y valida el valor esperado
func (f *Filters) ValidateValue(value string, typeValue string, pattern string) bool {

	var regExp string
	if value == "[NULL]" {
		return true
	}

	switch {
	case typeValue == "alphabetical":
		regExp = "^([a-zA-Z_-]|\\s)*$"

	case typeValue == "number":
		regExp = "^[0-9,.]*$"
	case typeValue == "text":
		regExp = "^([a-zA-Z0-9_á-úÁ-ÚñÑ -]|\\$)*$"

	case typeValue == "numeric":
		regExp = "^[0-9,]*$"

	case typeValue == "alphanumeric":
		regExp = "^([a-zA-Z0-9_-]|\\$)*$"


	case typeValue == "arrayint":
		regExp = "^[0-9](,[0-9])*$"

	case typeValue == "date":
		// regExp = "^(([123\\d{3})-(0[1-9]|1[0-2])-(0[1-9]|[12\\d|3[01]))*$"
		regExp = "^(([0-9]{4})-([0-9]{2})-([0-9]{2}))*$"
	case typeValue == "CustomSQL":
		regExp = pattern
	case typeValue == "pattern":
		regExp = pattern

	default:
		regExp = "^[a-zA-Z]*$"
	}

	reg := regexp.MustCompile(regExp)

	log.Printf("Filter: %v", reg.MatchString(value))
	return reg.MatchString(value)

}
