//package filter
package filter

//ValidFilter Setea el tipo de valor que puede tener un filtro
type ValidFilter struct {
	Key       string `json:"field"`
	Field     string `json:"-"`
	TypeKey   string `json:"type"`
	CustomSQL string `json:"-"`
	Pattern		string `json:"-"`
}

type Param struct {
	Key       string
	// SQL				string
	Value     interface{}
	Operator  string
	CustomSQL string
	Type      string
}
