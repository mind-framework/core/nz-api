//Package app el paquete application contiene todos los métodos y recursos comunes a la aplicación
package app

import (
	"github.com/rs/zerolog"
	log "github.com/rs/zerolog/log"
	"os"
	"time"
  "encoding/json"
  "io/ioutil"
	gorpc "net/rpc"

	// "gitlab.com/mind-framework/core/nz-api/app"

	"gitlab.com/mind-framework/core/nz-api/role"
	"gitlab.com/mind-framework/core/nz-api/rpc"

	"github.com/go-chi/jwtauth"
)

// var log = _log.With().Str("ctx", "app").Logger()

const (
	LogLevelDB   = 1
	LogLevelAuth = 2
	LogLevelHttp = 4
)

var (
	//TokenAuth Estructura para crear y validar token JWT
	TokenAuth *jwtauth.JWTAuth
	//Exp Tiempo de expiracion del JWT
	Exp time.Duration
	//Secret utilizado para la creacion de JWT
	Secret string
	//Role Lista de Roles por uris y permisos
	// Role role.Role
	//ModuleName del para la carga de permisos
	// ModuleName string
	Module ModuleInfo
	//Roles Lista de Roles agrupados por Permisos
	Roles Permisos
	//ListenerRPC Handler para escuchar peticiones RPC
	ListenerRPC rpc.HandlerRPC
	//ClientsRPC Map con los clientes de RPC conectados el modulo
	ClientsRPC map[string]*gorpc.Client
	//Uris Losta de uris del modulo
	Uris Routes
)

//Permisos struct utilizado para la carga de Recursos y perfiles
type Permisos struct {
	Recursos role.Modulo
	Perfiles map[string]uint64
}

func init() {
	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix
	zerolog.SetGlobalLevel(-1)
	log.Logger = log.With().Caller().Logger()
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})


	Module.APIVersion = os.Getenv("API_VERSION")

	Uris = make(map[string]Methods)
	Roles.Perfiles = make(map[string]uint64)
	ClientsRPC = make(map[string]*gorpc.Client)
	Exp = 600 * time.Minute

	Secret = os.Getenv("API_JWT_SECRET")

	TokenAuth = jwtauth.New("HS256", []byte(Secret), nil)

	//Inicio RPC

	go ListenerRPC.Listen()

	//CREAR ARCHIVO DE AUDITORIA PARA ESCRIBIR SI FALLA EL ENVIO RCP

}

//CargarPermisos Consoulta Perfiles y permisos por RCP al modulo CORE y lo carga en memoria cada 1 minuto.
func CargarPermisos() {
	if (os.Getenv("API_MODULE_CORE")!=""){
		clientRCP, err := ListenerRPC.Connect(os.Getenv("API_MODULE_CORE") + ":8767")
		if err != nil {
			log.Error().Err(err).Str("API_MODULE_CORE", os.Getenv("API_MODULE_CORE")).Msg("Error a conectar con RPC")
			time.Sleep(3000 * time.Millisecond)
			log.Warn().Msg("Reintentando conexion")
			CargarPermisos()

		}

		defer clientRCP.Close()

		for {

			clientRCP.Call("Perfiles.ObtenerPerfilesRPC", Module.ModuleName, &Roles.Perfiles)
			clientRCP.Call("Recursos.ObtenerPermisosRPC", Module.ModuleName, &Roles.Recursos)
			time.Sleep(60000 * time.Millisecond)
			//	log.Printf("Perfiles: %#v ", Roles.Perfiles)
		}
	}else{
		log.Warn().Msg("Cargando Perfiles y Roles de recursos locales")
    rolesJson, err := os.Open("standalone-dev/roles.json")
    if err != nil {
			log.Error().Err(err).Msg("Error al leer Roles")
			return
    }
    perfilesJson, err := os.Open("standalone-dev/perfiles.json")
    if err != nil {
			log.Error().Err(err).Msg("Error al leer Perfiles")
			return
    }

    defer rolesJson.Close()
    defer perfilesJson.Close()

    rolesValue, _ := ioutil.ReadAll(rolesJson)
    perfilesValue, _ := ioutil.ReadAll(perfilesJson)

    json.Unmarshal([]byte(perfilesValue), &Roles.Perfiles)
    json.Unmarshal([]byte(rolesValue), &Roles.Recursos)
	// log.Info().Interface(&Roles.Recursos)



  }
	// var ress rpc.Response

	// ress = rpc.Response
	// a.Call("Commands.Ping", "asd", &ress)

	// a.Close()
	// a.Call("Perfiles.ObtenerPerfilesRPC", app.ModuleName, &app.Roles.Perfiles)
	// a.Call("Recursos.ObtenerPermisosRPC", app.ModuleName, &app.Roles.Recursos)
	// var ress rpc.Response

	// ress = rpc.Response
	// a.Call("Commands.Ping", "asd", &ress)

	return
	// a.Close()

}

// type Request struct {
// 	A, B float64
// }
