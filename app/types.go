package app

import (
	"github.com/go-chi/chi"
)

//Routes Estructura que contiene las uris y verbos html de la api
type Routes map[string]Methods

type Methods struct {
	Methods []string
}

//PrintRoutes Metodo que recorre todas las rutas de un chi.Routes
func (ro Routes) PrintRoutes(r chi.Routes, indent string, prefix string) Routes {

	routes := r.Routes()

	for _, route := range routes {
		if route.SubRoutes != nil && len(route.SubRoutes.Routes()) > 0 {

			Uris = ro.PrintRoutes(route.SubRoutes, indent, prefix+route.Pattern[:len(route.Pattern)-2])

		} else {
			var m Methods
			for key, _ := range route.Handlers {

				m.Methods = append(m.Methods, key)

			}
			Uris[prefix+route.Pattern] = m

		}

	}

	return Uris
}

//ModuleInfo Estructura utilizada para guardar informacion del modulo
type ModuleInfo struct {
	ModuleName string `json:"moduleName"`
	APIBuild   string `json:"apiBuild"`
	APIVersion string `json:"apiVersion"`
}

//Interface y estructuras de error

type handlerError interface {
	Error() string
}

//HandlerError Estructura que implementa interface de error y handlerError, que agrega codigo de error
type HandlerError struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

//Error metodo que devuelve el mensaje de error
func (e *HandlerError) Error() string {
	return e.Message
}
