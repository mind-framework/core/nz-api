package rpc

import (
	"log"
)

// import (
// "gitlab.com/mind-framework/core/nz-api/app"
// "gitlab.com/mind-framework/core/nz-api/role"
// )

type Commands struct {
	Response
}

type Response struct {
	Code    int
	Message string
	// Data    map[string]string
}

//Ping metodo utilizado para saber si el modulo se encuentra en ejecucion
func (c *Commands) Ping(arg string, rp *Response) error {
	log.Printf("<- RPC Command Ping: %v %v", arg, rp)
	rp.Code = 1
	rp.Message = "UP"
	log.Printf("-> RPC Command Ping: %v", rp)
	return nil
}
