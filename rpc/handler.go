package rpc

import (
	"log"
	"net"
	gorpc "net/rpc"
)

type HandlerRPC struct {
	inbound *net.TCPListener
}

func init() {
	log.Printf("Iniciando RPC.....")
}

//Listen Metodo para iniciar el servicio RPC
func (r *HandlerRPC) Listen() {

	addy, err := net.ResolveTCPAddr("tcp", "0.0.0.0:8767")
	if err != nil {
		log.Fatal(err)

	}

	r.inbound, err = net.ListenTCP("tcp", addy)
	if err != nil {
		log.Fatal(err)

	}

	// listener := new(recursos.LocalHandler)
	response := new(Commands)
	// gorpc.Register(listener)
	gorpc.Register(response)
	gorpc.Accept(r.inbound)

}

func (r *HandlerRPC) RegistrarComando(command interface{}) {

	gorpc.Register(command)
	// gorpc.Accept(r.inbound)

}

func (r *HandlerRPC) Accept() {

	//	gorpc.Register(command)
	gorpc.Accept(r.inbound)

}

func (r *HandlerRPC) Connect(address string) (*gorpc.Client, error) {

	client, err := gorpc.Dial("tcp", address)
	if err != nil {
		log.Fatal(err)
		return nil, err
	}

	// err = client.Call("Respuesta.GetPermisos", "nwc-oc", &)
	// 	if err != nil {
	// 		log.Fatal(err)
	// }
	return client, nil
}

//Declaracion de comamndos comunes a todas las apis

// type Respuesta int

// func (r *Respuesta)ObtenerPermisos(moduleName string, rp *role.Roles) error {

// 	log.Printf("RPC establecido")
// 	 rp =app.Roles[moduleName]
// //	recursos.NewHandler().ObtenerPermisos(&moduleName, rp)
// 	return nil
// }

// func Listen(){

// 	addy, err := net.ResolveTCPAddr("tcp", "0.0.0.0:8767")
// 	if err != nil {
// 		log.Fatal(err)
// 	}

// 	inbound, err := net.ListenTCP("tcp", addy)
// 	if err != nil {
// 		log.Fatal(err)
// 	}

// 	// listener := new(recursos.LocalHandler)
// 	r := new(Respuesta)
// 	// gorpc.Register(listener)
// 	gorpc.Register(r)
// 	gorpc.Accept(inbound)
// }
