package handler

import (
	"database/sql"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	// "reflect"
	"encoding/json"
	"github.com/rs/zerolog/log"
	"regexp"

	// "github.com/go-chi/chi"
	"gitlab.com/mind-framework/core/nz-api/app"
	"gitlab.com/mind-framework/core/nz-api/filter"
	"gitlab.com/mind-framework/core/nz-api/middleware"
	"gitlab.com/mind-framework/core/nz-api/model"
)

//GenericHandler Handler generico para peticiones HTTP
type GenericHandler struct {
	//IHandler
	Recurso      string
	Model        model.Modeler
	Models       []model.Modeler
	InstanceName string
}

//Metadata Informacion adicional del recurso
type Metadata struct {
	Total       int                  `json:"total"`
	Filters     []filter.ValidFilter `json:"filtros"`
	Fields      []interface{}        `json:"fields"`
	Permissions interface{}          `json:"permisos"`
	Errors      []string             `json:"errors"`
}

//Get Retorna un recurso de tipo GenericHandler.Model
func (g *GenericHandler) Get(pk string, w http.ResponseWriter, r *http.Request) {

	if pk == "" {
		pk = "id"
	}

	meta := Metadata{}
	rp := middleware.ContextParams(r)
	meta.Permissions = rp.Permisos
	filters := g.Model.ValidFilters()
	modelKeys := g.GetParams(r)
	if len(modelKeys[pk]) > 0 {
		switch modelKeys[pk][0] {
		case "":
			app.Error(w, "Se esperaba un id de recurso")
			return
		case "_self":
			modelKeys["username"] = []string{rp.UserInfo.UserName}
		}
	}

	//En el GET siempre retorno el modelo extendido
	extended := false
	if rp.Permisos.Esp2 {
		extended = true
	}

	sort, _ := rp.Sort.Sql("mysql", false)

	scope := model.ORM.Order(sort).Limit(rp.Limit.Count).Set("gorm:auto_preload", true)
	if rp.Limit.Offset != 0 {
		scope = scope.Offset(rp.Limit.Offset)
	}
	//Agrego  Filtros
	if len(filters) > 0 {
		var f = new(filter.Filters)

		f.AcceptParams = filters

		if ok, err := f.SetParams(modelKeys); !ok {
			app.Error(w, err.Error(), 400)
			return
		}

		for _, b := range f.Params {
			if b.Value != nil && b.Value != "" {
				scope = scope.Where(b.CustomSQL, b.Value)
			} else {
				scope = scope.Where(b.CustomSQL)
			}
		}

	}

	m := g.Model.NewModel(modelKeys, extended)
	var count int

	//Realizo la consulta y verifico si obtuve resultados.
	errs := scope.Find(m).Count(&count).Error

	if count == 0 {
		m = nil
		app.Error(w, "Recurso no encontrado", 404)
		return
	}
	//Valido si hay algun error distinto de recurso no encontrado.
	if errs != nil && errs.Error() != "record not found" {

		app.Error(w, errs, 500)
		return

	}

	//Verifico si recibi algun valor, sino limpio el model a retornar
	log.Printf("Count: %v", count)
	if count > 1 {
		m = nil
		app.Error(w, "ID Invalido, retorna multiples registros", 400)
		return
	}
	meta.Total = count
	meta.Filters = filters
	//Retorno un success con el modelo incluido
	app.Success(w, m, meta)

}

//GetAll Retorna un array con recursos del tipo GenericHandler.Models
func (g *GenericHandler) GetAll(w http.ResponseWriter, r *http.Request) {
	modelKeys := g.GetParams(r)

	var count int
	filters := g.Model.ValidFilters()
	rp := middleware.ContextParams(r)

	meta := Metadata{}
	meta.Filters = filters
	meta.Permissions = rp.Permisos
	// log.Printf("rp: %#v", rp.Permisos)
	// reqRol := rp.UserInfo.Rol
	// Rol, _ := app.Roles.Perfiles[reqRol]
	// module, _ := app.Roles.Recursos[app.Module.ModuleName]
	// PEspecial1 := module[g.Recurso].Perfiles_E1
	// PEspecial2 := module[g.Recurso].Perfiles_E2
	// log.Printf("ESP1: %v", rp.Permisos.Esp1)
	//Valido si tiene persmisos especiales para retornar el modelo extendido.
	extended := false
	if rp.Permisos.Esp2 {
		extended = true
	}

	list := g.Model.NewModelList(extended)

	//preparo el order by y el limit
	sort, _ := rp.Sort.Sql("mysql", false)

	if rp.Limit.Count == -1 {
		rp.Limit.Count = 1000
	}
	scope := model.ORM.Order(sort).Limit(rp.Limit.Count)
	if rp.Limit.Offset != 0 {
		log.Print("Seteando OFFSET")
		scope = scope.Offset(rp.Limit.Offset)
	}
	//Agrego  Filtros
	if len(filters) > 0 {
		var f = new(filter.Filters)

		// scope2 := model.ORM.Or("id_nodo is null").Or("id_nodo = ''")

		f.AcceptParams = filters

		// f.SetParams(modelKeys)
		if ok, err := f.SetParams(modelKeys); !ok {
			app.Error(w, err.Error(), 400)
			return
		}

		for _, b := range f.Params {
			if b.Value != nil {
				scope = scope.Where(b.CustomSQL, b.Value)
			} else {
				scope = scope.Where(b.CustomSQL)
			}
		}

	}

	//GROUP
	var sqlGroup string
	var res []map[string]interface{}
	if len(modelKeys["groupby"]) > 0 {
		// n:= g.Model.NewModelList(true)
		sqlGroup = strings.Join(modelKeys["groupby"], ",")
		var campos string
		var camposSlice []string
		regExp := "^([a-zA-Z0-9_]|\\$)*$"
		camposSlice = append(camposSlice, sqlGroup)
		switch t := modelKeys; {
		case len(t["sum"]) > 0:
			for i := range t["sum"] {
				reg := regexp.MustCompile(regExp)
				if reg.MatchString(t["sum"][i]) {
					camposSlice = append(camposSlice, "sum("+t["sum"][i]+") as "+t["sum"][i])
				}
			}
			break

		case len(t["avg"]) > 0:
			for i := range t["avg"] {
				reg := regexp.MustCompile(regExp)
				if reg.MatchString(t["avg"][i]) {
					camposSlice = append(camposSlice, "avg("+t["avg"][i]+") as "+t["avg"][i])
				}
			}
			break

		case len(t["max"]) > 0:
			for i := range t["max"] {
				reg := regexp.MustCompile(regExp)
				if reg.MatchString(t["max"][i]) {
					camposSlice = append(camposSlice, "max("+t["max"][i]+") as "+t["max"][i])
				}
			}
			break

		case len(t["min"]) > 0:
			for i := range t["min"] {
				reg := regexp.MustCompile(regExp)
				if reg.MatchString(t["min"][i]) {
					camposSlice = append(camposSlice, "min("+t["min"][i]+") as "+t["min"][i])
				}
			}
			break

		}
		if len(camposSlice) > 0 {
			campos = strings.Join(camposSlice, ",")
		}

		rows, e := scope.Table(g.Model.TableName()).Select(campos + " ,count(*) as count ").Group(sqlGroup).Rows()

		if e != nil {
			log.Error().Err(e)
			app.Error(w, e.Error(), 400)
			return

		}
		defer rows.Close()

		e = model.MapScanner(rows, &res)
		if e != nil {
			log.Error().Err(e)
			app.Error(w, "Error al procesar los datos", 500)
			return
		}
		app.Success(w, &res, meta)

	} else {

		scope.Set("gorm:auto_preload", true).Find(list).Count(&count)
		meta.Total = count
		app.Success(w, list, meta)

	}
	return

}

//Update Modifica los datos de un recurso de timpo GenericHandler.Model
func (g *GenericHandler) Update(pk string, w http.ResponseWriter, r *http.Request) {
	if pk == "" {
		pk = "id"
	}

	rp := middleware.ContextParams(r)

	// id := rp.Chi.URLParam(pk)
	reqRol := rp.UserInfo.Rol
	Rol, _ := app.Roles.Perfiles[reqRol]
	module, _ := app.Roles.Recursos[app.Module.ModuleName]
	// PEspecial1 := module[g.Recurso].Perfiles_E1
	PEspecial2 := module[g.Recurso].Perfiles_E2

	modelKeys := g.GetParams(r)

	if len(modelKeys[pk]) > 0 {
		switch modelKeys[pk][0] {
		case "":
			app.Error(w, "Se esperaba un id de recurso")
			return
		case "_self":
			modelKeys["username"] = []string{rp.UserInfo.UserName}
		}
	}

	//Valido si tiene persmisos especiales para retornar el modelo extendido.
	extended := false
	if Rol&PEspecial2 != 0 {
		extended = true
	}
	m := g.Model.NewModel(modelKeys, extended)
	if rp.RequestBody.Data == nil {
		app.Fail(w, "No hay datos que modificar", 400)
		return
	}
	//Modelo utilizado para completar con los datos recibidos del Request
	n := g.Model.NewModel(modelKeys, extended)
	jsonbody, err := json.Marshal(rp.RequestBody.Data)
	if err != nil {
		// do error check
		fmt.Println(err)
		return
	}
	if err := json.Unmarshal(jsonbody, n); err != nil {
		// do error check
		fmt.Println(err)
		return
	}
	//actualizo el modelo
	if code, err := model.Update(m, n, true); err != nil {
		app.Error(w, err.Error(), code)
		return
	}
	app.Success(w, m, "")

}

//Create Crea uno o mas recursos del tipo GenericHandler.Models
func (g *GenericHandler) Create(w http.ResponseWriter, r *http.Request) {

	rp := middleware.ContextParams(r)
	// modelKeys := g.getParams(r)

	reqRol := rp.UserInfo.Rol
	Rol, _ := app.Roles.Perfiles[reqRol]
	module, _ := app.Roles.Recursos[app.Module.ModuleName]
	// PEspecial1 := module[g.Recurso].Perfiles_E1
	PEspecial2 := module[g.Recurso].Perfiles_E2

	var resp []interface{}
	var pks map[string][]string
	pks = make(map[string][]string, 1)

	var errs []Errors
	errorFound := 0

	for _, b := range rp.RequestBody.Data.([]interface{}) {

		//Valido si tiene persmisos especiales para retornar el modelo extendido.
		extended := false
		if Rol&PEspecial2 != 0 {
			extended = true
		}

		// for _, b := range rp.RequestBody.Data.([]interface{}) {
		m := g.Model.NewModel(pks, extended)
		js, err := json.Marshal(b)

		if err != nil {
			continue
		}

		json.Unmarshal(js, &m)
		// model.ORM.Create(m)
		code, err := model.Create(m)

		e := Errors{Code: 200, Message: ""}

		if err != nil {

			errorFound++
			e.Message = err.Error()
			e.Code = code

			// app.Error(w, err.Error(), code)
			// return
		}
		errs = append(errs, e)

		resp = append(resp, m.(interface{}))

	}

	if errorFound == 0 {

		app.Success(w, resp, "")
		return

	} else {

		if errorFound == len(rp.RequestBody.Data.([]interface{})) {

			app.Error(w, errs[0].Message)

			return

		} else {

			app.SuccessWithWarning(w, resp, "", "", errs)
			return
		}
	}

}

//Delete Sirve para eliminar un recurso del tipo GenericHandler.Model
func (g *GenericHandler) Delete(pk string, w http.ResponseWriter, r *http.Request) {
	var err error
	if pk == "" {
		pk = "id"
	}

	rp := middleware.ContextParams(r)

	reqRol := rp.UserInfo.Rol
	Rol, _ := app.Roles.Perfiles[reqRol]
	module, _ := app.Roles.Recursos[app.Module.ModuleName]
	// PEspecial1 := module[g.Recurso].Perfiles_E1
	PEspecial2 := module[g.Recurso].Perfiles_E2

	modelKeys := g.GetParams(r)
	if len(modelKeys[pk]) > 0 {
		switch modelKeys[pk][0] {
		case "":
			app.Error(w, "Se esperaba un id de recurso")
			return
		case "_self":
			modelKeys["username"] = []string{rp.UserInfo.UserName}
		}
	}

	//Valido si tiene persmisos especiales para retornar el modelo extendido.
	extended := false
	if Rol&PEspecial2 != 0 {
		extended = true
	}

	m := g.Model.NewModel(modelKeys, extended)

	//Ejecutando hook BeforeDelete
	err = g.BeforeDelete(modelKeys, r)
	if err != nil {
		app.Error(w, err.Error(), 500)
		return
	}
	var code int
	code, err = model.Delete(m)
	if err != nil {
		app.Error(w, err.Error(), code)
		return
	}

	err = g.AfterDelete(modelKeys, r)
	if err != nil {
		app.Error(w, err.Error(), 500)
	}

	app.Success(w, "", "")
}

//NotImplented Sirve para retornar una respuesta para los verbos HTML no implementados
func (g *GenericHandler) NotImplented(w http.ResponseWriter, r *http.Request) {
	app.Error(w, "No implementado!", http.StatusNotImplemented)
	return
}

//GetParams ...
func (g *GenericHandler) GetParams(r *http.Request) map[string][]string {

	rp := middleware.ContextParams(r)
	urlParamsKeys := rp.Chi.URLParams.Keys
	urlQueryParams := rp.QueryParams
	var modelKeys map[string][]string
	modelKeys = make(map[string][]string)

	for _, b := range urlParamsKeys {

		if b != "*" {

			modelKeys[b] = append(modelKeys[b], rp.Chi.URLParam(b))

		}
	}
	for k, v := range urlQueryParams {

		modelKeys[k] = v
		log.Printf("K: %#v", k)
		log.Printf("V: %#v", v)

	}
	log.Printf("modelKeys %v", modelKeys)
	return modelKeys
}

//UploadFiles handler para subir archivos al servidor.
func (g *GenericHandler) UploadFiles(r *http.Request) error {

	rp := middleware.ContextParams(r)

	if len(rp.Files) == 0 {

		return errors.New("Error al leer archivos")
	}

	for _, file := range rp.Files {

		log.Printf("Extension: %v", filepath.Ext(file.Filename))

		// log.Printf("Size: %#v", file.Header.Get("filename"))

		tempFile, err := ioutil.TempFile(os.Getenv("API_TMP_FOLDER"), "upload-*"+filepath.Ext(file.Filename))

		if err != nil {
			log.Error().Err(err)
			_, fileError := os.Stat(tempFile.Name())
			if os.IsExist(fileError) {
				tempFile.Close()
				os.Remove(tempFile.Name())
			}
			continue
			// log.Println("Error: %#v", err)

		}

		defer tempFile.Close()

		fileBytes, err := ioutil.ReadAll(file.File)

		if err != nil {
			log.Error().Err(err)
			_, fileError := os.Stat(tempFile.Name())
			if os.IsExist(fileError) {
				os.Remove(tempFile.Name())
			}
			continue
		}
		// write this byte array to our temporary file
		tempFile.Write(fileBytes)

	}

	// return that we have successfully uploaded our file!
	//fmt.Fprintf(w, "Successfully Uploaded File\n")

	return nil

}

// func (g *GenericHandler) Routes() chi.Router {

// 	r := chi.NewRouter()
// 	r.Get("/", g.GetAll)
// 	r.Post("/", g.Create)
// //	r.Put("/", g.Update)

// 	//	r.Get("/:self", h.GetSelf) //ruta para mostrar los datos del usuario actual

// 	//ignoro solicitudes no soportadas a :self
// 	// r.Post("/_self", h.NotImplented)
// 	// r.Delete("/self", h.NotImplented)
// 	// r.Put("/_self", h.NotImplented)

// 	r.Route("/{id}", func(r chi.Router) {
// 		r.Use()
// 		r.Get("/*", g.Get)
// 		r.Patch("/", g.Update)
// 		// r.Put("/", )
// 		r.Delete("/", g.Delete)
// 	})

// 	return r

// 	// r := chi.NewRouter()

// 	// r.Get("/{id}", g.Get)
// 	// r.Patch("/{id}", g.Update)
// 	// r.Get("/", g.GetAll)
// 	// r.Delete("/{id}", g.Delete)
// 	// r.Post("/", g.Create)

// 	// return r
// }

// func (g *GenericHandler) getModel(m model.Modeler) model.Modeler {

// 	tModel := reflect.TypeOf(g.Model).Elem()
// 	key := string(g.Model.Keys()[0])
// 	m := reflect.New(tModel)
// 	// m.Elem().FieldByName(key).SetString(id)
// 	em := m.Interface().(model.Modeler)

// 	// return model.Modeler
// 	return em

// }
func (g *GenericHandler) checkCount(rows *sql.Rows) (count int) {
	for rows.Next() {
		rows.Scan(&count)

	}
	return count
}

//BeforeDelete Hook ejecutado antes de la eliminacion de un registro
func (g *GenericHandler) BeforeDelete(modelkeys map[string][]string, r *http.Request) (err error) {

	return err
}

//AfterDelete Hook ejecutado despues de la eliminacion de un registro
func (g *GenericHandler) AfterDelete(modelkeys map[string][]string, r *http.Request) (err error) {
	return err
}
