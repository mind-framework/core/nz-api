package handler
//Errors Estructura para retornar errores
type Errors struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

