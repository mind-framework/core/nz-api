package handler

import (
	"net/http"

	"github.com/go-chi/chi"
	mdw "gitlab.com/mind-framework/core/nz-api/middleware"
)

//Routes retorna las rutas por default del GenericHandler
func (g *GenericHandler) Routes(auth bool, middleware ...func(http.Handler) http.Handler) chi.Router {

	r := chi.NewRouter()
	if auth {
		r.Use(mdw.Authorization(nil, g.Recurso))

	}

	//Si recibi un middleware o mas lo cargo en las rutas
	if middleware != nil {

		for _, m := range middleware {

			r.Use(m)
		}

	}

	r.Get("/", g.GetAll)
	r.Post("/", g.Create)
	r.Put("/", g.NotImplented)

	//	r.Get("/:self", h.GetSelf) //ruta para mostrar los datos del usuario actual
	//ignoro solicitudes no soportadas a :self
	r.Post("/_self", g.NotImplented)
	r.Delete("/self", g.NotImplented)
	r.Put("/_self", g.NotImplented)

	r.Route("/{id}", func(r chi.Router) {

		//r.Get("/*", g.Get("id"))
		r.Get("/*", func(w http.ResponseWriter, r *http.Request) {
			//	if validaPermisos("READ")
			g.Get("id", w, r)

		})

		r.Patch("/", func(w http.ResponseWriter, r *http.Request) {
			g.Update("id", w, r)
		})

		r.Put("/", g.NotImplented)

		r.Delete("/", func(w http.ResponseWriter, r *http.Request) {
			g.Delete("id", w, r)
		})

		//	r.Delete("/", g.Delete)
	})

	return r

}

func (g *GenericHandler) validaPermisos(permiso string) {

	//

}
