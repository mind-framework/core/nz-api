package model

import (
	//	"bytes"
	//	"errors"
	//	"database/sql"
	"database/sql/driver"
	"log"
	"fmt"
	"encoding/json"
	"strings"
	"errors"

	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/base64"
	"os"
	"io"
)

//WeekDays - Estructura con los días de la semana
type WeekDays struct {
	Mon bool
	Tue bool
	Wed bool
	Thu bool
	Fri bool
	Sat bool
	Sun bool
}

//Value - Devuelve los días de la semana como JSON
func (wd *WeekDays) Value() (driver.Value, error) {
	if wd.IsNull() {
		return nil, nil
	}

	return json.Marshal(wd)
}

//Scan - lee los días de la seman desde un json o de un array
func (wd *WeekDays) Scan(value interface{}) error {

	if b, ok := value.([]byte); ok == true {
		json.Unmarshal(b, &wd)
	} else {
		wd.fromSlice(value)

		//		m,_ := json.Marshal(value)
		//		json.Unmarshal(m, j)
	}

	return nil
}

//MarshalJSON Devuelve una representación JSON de los dias de la semana
func (wd WeekDays) MarshalJSON() ([]byte, error) {
	var i []string
	if wd.Mon {
		i = append(i, "Lun")
	}
	if wd.Tue {
		i = append(i, "Mar")
	}
	if wd.Wed {
		i = append(i, "Mié")
	}
	if wd.Thu {
		i = append(i, "Jue")
	}
	if wd.Fri {
		i = append(i, "Vie")
	}
	if wd.Sat {
		i = append(i, "Sáb")
	}
	if wd.Sun {
		i = append(i, "Dom")
	}

	return json.Marshal(i)

}

//UnmarshalJSON método que realiza la conversión desde JSON
func (wd *WeekDays) UnmarshalJSON(data []byte) error {
	var list interface{}

	*wd = WeekDays{false, false, false, false, false, false, false}

	if err := json.Unmarshal(data, &list); err == nil {
		wd.fromSlice(list)

	}

	return nil
}

/*
	carga la estructura desde una lista de días recibidas en un slice
	el formato de list debe ser un slice con el formato: ["Lun", ... ,"Dom"]
*/
func (wd *WeekDays) fromSlice(list interface{}) error {

	*wd = WeekDays{false, false, false, false, false, false, false}

	for _, c := range list.([]interface{}) {
		switch strings.ToLower(c.(string)) {
		case "lun", "lunes", "mon", "monday":
			wd.Mon = true
		case "mar", "martes", "tue", "tuesday":
			wd.Tue = true
		case "mie", "mié", "miercoles", "miércoles", "wed", "wednesday":
			wd.Wed = true
		case "jue", "jueves", "thu", "thursday":
			wd.Thu = true
		case "vie", "viernes", "fri", "friday":
			wd.Fri = true
		case "sab", "sáb", "sábado", "sat", "saturday":
			wd.Sat = true
		case "dom", "domingo", "sun", "sunday":
			wd.Sun = true

		}
	}

	return nil
}

//IsNull devuelve true si el campo es nulo
func (wd WeekDays) IsNull() bool {
	return !wd.Mon && !wd.Tue && !wd.Wed && !wd.Thu && !wd.Fri && !wd.Sat && !wd.Sun
}

//JSON - Tipo de datos objeto JSON
//Ejemplo: "personas":{"nombre":"valor",
//					   "apellido":"valor"}
type JSONOBJECT map[string]interface{}

//Value devuelve la versión json de la variable
func (j JSONOBJECT) Value() (driver.Value, error) {
	if j.IsNull() {
		return nil, nil
	}
	
	return json.Marshal(j)
}

//Scan -
func (j *JSONOBJECT) Scan(value interface{}) error {

	if b, ok := value.([]byte); ok == true {
		json.Unmarshal(b, &j)

	}

	return nil
}

//IsNull - Devuelve true si la variable está vacía
func (j JSONOBJECT) IsNull() bool {
	return j == nil || len(j) == 0
}



type JSONENCRYPT map[string]interface{}

func (je JSONENCRYPT) Value() (driver.Value, error) {
	if je.IsNull() {
		return nil, nil
	}
	key:= os.Getenv("API_AES_KEY")

	cadena, e:= json.Marshal(je)

	cyper, e:= je.encrypt([]byte(key),string(cadena))

	return []byte(cyper), e
}

//Scan -Retorna el dato de la BD
func (je *JSONENCRYPT) Scan(value interface{}) error {
	
	key:= os.Getenv("API_AES_KEY")
	
	cadena, err:= je.decrypt([]byte(key),string(value.([]byte)))
	if err!= nil {
		return err
	}
	
		json.Unmarshal([]byte(cadena), &je)


	return nil
}

//IsNull - Devuelve true si la variable está vacía
func (je JSONENCRYPT) IsNull() bool {
	return je == nil || len(je) == 0
}

// encrypt string a base64 crypto usando AES
func (je JSONENCRYPT)encrypt(key []byte, text string) (string,error) {

	plaintext := []byte(text)
	
	block, err := aes.NewCipher(key)
	if err != nil {
		return "", err
	}

	
	ciphertext := make([]byte, aes.BlockSize+len(plaintext))
	iv := ciphertext[:aes.BlockSize]
	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		return "", err
	}

	stream := cipher.NewCFBEncrypter(block, iv)
	stream.XORKeyStream(ciphertext[aes.BlockSize:], plaintext)

	// convert to base64
	return base64.URLEncoding.EncodeToString(ciphertext), nil
}

// decrypt desde base64 a string desencriptado
func (je JSONENCRYPT)decrypt(key []byte, cryptoText string) (string,error) {
	ciphertext, _ := base64.URLEncoding.DecodeString(cryptoText)

	block, err := aes.NewCipher(key)
	if err != nil {
		return "", err
	}

	if len(ciphertext) < aes.BlockSize {
		return "", errors.New("ciphertext too short")
	}
	iv := ciphertext[:aes.BlockSize]
	ciphertext = ciphertext[aes.BlockSize:]

	stream := cipher.NewCFBDecrypter(block, iv)

	
	stream.XORKeyStream(ciphertext, ciphertext)
	log.Printf("%#v",ciphertext)
	return fmt.Sprintf("%s", ciphertext), nil
}











//JSONSLICE Tipo de dato array JSON
//Ejemplo: [{"nombre":"valor","apellido":"valor"},{"nombre":"valor","apellido":"valor"}]
type JSONSLICE []map[string]interface{}

//Value devuelve la versión json de la variable
func (j JSONSLICE) Value() (driver.Value, error) {

	if j.IsNull() {
		return nil, nil
	}

	return json.Marshal(j)
}

//Scan - devuelve un array de un objeto json
func (j *JSONSLICE) Scan(value interface{}) error {

	if b, ok := value.([]byte); ok == true {
		json.Unmarshal(b, &j)

	} else {
		var m []byte
		var err2 error

		if m, err2 = json.Marshal(value); err2 == nil {
			json.Unmarshal(m, &j)

		} else {
			log.Printf("Error: %v", err2)
		}

	}

	return nil
}

//IsNull - Devuelve true si la variable está vacía
func (j JSONSLICE) IsNull() bool {
	return j == nil || len(j) == 0
}

//UriMap tipo de dato para almacenar Uris
type UriMap map[string]Uri

//Profile tipo de dato para almacenar los Roles
type Profile map[string]Uri

//Uri Tipo de dato para almacenar Uris y Permisos
type Uri map[string]Permiso

//Permiso Tipo de dato para almacenar permisos de Uris
type Permiso struct {
	READ   bool `json:"read"`
	WRITE  bool `json:"write"`
	APPEND bool `json:"append"`
	DELETE bool `json:"delete"`
	ESP1   bool `json:"esp1"`
	ESP2   bool `json:"esp2"`
}

// StringSlice es un type []string para utilizar con JSON
type StringSlice []string

//Value devuelve la versión json de la variable
func (s StringSlice) Value() (driver.Value, error) {

	if s.IsNull() {
		return nil, nil
	}

	return json.Marshal(s)
}

//Scan - devuelve un array de un objeto json
func (s *StringSlice) Scan(value interface{}) error {

	if b, ok := value.([]byte); ok == true {
		json.Unmarshal(b, &s)

	} else {
		var m []byte
		var err2 error

		if m, err2 = json.Marshal(value); err2 == nil {
			json.Unmarshal(m, &s)

		} else {
			log.Printf("Error: %v", err2)
		}

	}

	return nil
}

//IsNull - Devuelve true si la variable está vacía
func (s StringSlice) IsNull() bool {
	return s == nil || len(s) == 0
}

//IntSlice  es un type []string para utilizar con JSON
type IntSlice []int

//Value devuelve la versión json de la variable
func (i IntSlice) Value() (driver.Value, error) {

	if i.IsNull() {
		return nil, nil
	}

	return json.Marshal(i)
}

//Scan - devuelve un array de un objeto json
func (i *IntSlice) Scan(value interface{}) error {

	if b, ok := value.([]byte); ok == true {
		json.Unmarshal(b, &i)

	} else {
		var m []byte
		var err2 error

		if m, err2 = json.Marshal(value); err2 == nil {
			json.Unmarshal(m, &i)

		} else {
			log.Printf("Error: %v", err2)
		}

	}

	return nil
}

//IsNull - Devuelve true si la variable está vacía
func (i IntSlice) IsNull() bool {
	return i == nil || len(i) == 0
}
