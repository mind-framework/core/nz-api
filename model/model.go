//Package model project model.go
package model

import (
	"database/sql"
	"errors"
	"fmt"
	"os"
	"strconv"

	// "gitlab.com/mind-framework/core/nz-api/app"

	// "reflect"
	"time"

	"github.com/jinzhu/gorm"
	"gitlab.com/mind-framework/core/nz-api/filter"
)

var (
	//ORM Referencia al ORM
	ORM             *gorm.DB
	TableNamePrefix string
)

//LogLevelDB ...
const LogLevelDB = 1

//Configure - Configura los parámetros del ORM
func Configure(db *gorm.DB) {
	ORM = db
	if os.Getenv("API_TABLEPREFIX") != "" {

		TableNamePrefix = os.Getenv("API_TABLEPREFIX") + "_"
	}
	logLevel := os.Getenv("LOG_LEVEL")
	if i, err := strconv.Atoi(logLevel); err == nil {
		if i < -1 {
			fmt.Printf("Habilitando Log de Base de Datos\n")
			db.LogMode(true)
		} else {
			fmt.Printf("Aviso: Log SQL deshabilitado. Para habilitarlo establecer variable de entorno LOG_LEVEL=-2\n")
		}
	} else {
		fmt.Printf("Aviso: Log SQL deshabilitado. Para habilitarlo establecer variable de entorno LOG_LEVEL=-2\n")
	}
}

//Register - Registra el modelo a utilizar
func Register(m Modeler) {
	//ORM.AutoMigrate(m)

	// if err := ORM.AutoMigrate(m).Error; err != nil {
	// 	fmt.Println(err)
	// } else {
	// 	fmt.Printf("AutoMigrando tabla '%v' ... \n", m.TableName())
	// }
	m.Init()

}

//BaseModel Campos por defecto a utilizar en todos los modelos
type BaseModel struct {
	CreatedAt time.Time  `json:"-"`
	UpdatedAt time.Time  `json:"-"`
	DeletedAt *time.Time `sql:"index" json:"-"`
}

//Modeler - Interface para Estructuras que deben ser tratadas como modelos
type Modeler interface {
	Keys() []string
	TableName() string
	GetError(int, error) (int, error)
	SetPkValue(map[string][]string) error
	NewModel(map[string][]string, bool) Modeler
	NewModelList(bool) interface{}
	ValidFilters() []filter.ValidFilter
	Init()
}

//Get - Carga los datos desde el origen de datos y los carga en el Modelo
func Get(m Modeler, id ...interface{}) (int, error) {
	//TODO: Debería chequear que la(s) clave(s) primaria(s) no esté(n) en blanco
	//		Si están en blanco se debe generar un error

	//scope := ORM.Where(m).Find(m).Limit(2)

	scope := ORM.Set("gorm:auto_preload", true).Where(m).Find(m).Limit(2)
	if err := scope.Error; err != nil {
		switch {
		case scope.RecordNotFound():
			return m.GetError(404, err)
		default:
			return m.GetError(500, err)
		}
	}
	if scope.RowsAffected > 1 {
		return m.GetError(500, errors.New("Multiples registros recibidos, se esperaba sólo uno"))
	}
	return 200, nil
}

//Update - Actualiza un registro
// el parámetro preLoad indica si se debe hacer un Get del recurso antes de realizar el update
func Update(m Modeler, value interface{}, preLoad bool) (int, error) {
	//Obtengo registro
	if preLoad {
		if code, err := Get(m); err != nil {
			return code, err
		}
	}
	scope := ORM.Model(m).
		Set("gorm:association_save_reference", false).
		Set("gorm:save_associations", false).
		Updates(value)

	if err := scope.Error; err != nil {
		switch {
		case scope.RecordNotFound():
			return m.GetError(404, err)
		default:
			return m.GetError(500, err)
		}
	}
	// association_save_reference:false;save_associations:false;
	//TODO: Ver si es necesario volver a cargar el registro

	return 200, nil
}

//Create - Crea un registro en la BD
func Create(m interface{}) (int, error) {

	//TODO: verificar que el usuario no exista antes

	scope := ORM.
		Set("gorm:association_save_reference", false).
		Set("gorm:save_associations", false).
		Create(m)
	//TODO: filtrar errores comunes
	if err := scope.Error; err != nil {

		switch {
		default:
			// return m.GetError(500, err)
			return 500, err
		}

	}

	return 200, nil
}

//Delete - Elimina Recurso
func Delete(m Modeler) (int, error) {
	//Obtengo el recurso
	if code, err := Get(m); err != nil {
		return code, err
	}

	scope := ORM.Delete(m)
	//TODO: filtrar errores comunes
	if err := scope.Error; err != nil {
		switch {
		default:
			return m.GetError(500, err)
		}
	}

	return 200, nil
}

//AddIndexFullText Metodo para validar si existe un indice FULLTEXT y su creacion
func AddIndexFullText(m Modeler, nameIndex, columns string) error {
	if nameIndex == "" || columns == "" {
		return nil
	}
	type resultado struct {
		KeyName string `gorm:column:Key_name`
	}
	var result []resultado

	ORM.Raw(fmt.Sprintf("SHOW INDEXES FROM %s WHERE key_name='%s' ", m.TableName(), nameIndex)).Scan(&result)
	if len(result) > 0 {
		return nil
	}

	ORM.Exec(fmt.Sprintf("CREATE FULLTEXT INDEX %s ON %s(%s)", nameIndex, m.TableName(), columns))

	return nil
}

//MapScanner Recibe rows y lo parsea en un map
func MapScanner(rows *sql.Rows, result *[]map[string]interface{}) error {

	var res []map[string]interface{}
	colsTypes, _ := rows.ColumnTypes()
	// log.Printf("ColumnsTypes %#v",colsTypes[0])

	for rows.Next() {

		cols, e := rows.Columns()
		if e != nil {
			return e
		}
		var refValores []interface{}
		valores := make([]string, len(cols))

		for i := range cols {

			refValores = append(refValores, &valores[i])
		}

		rows.Scan(refValores...)
		registro := make(map[string]interface{})
		for i, field := range cols {
			var v interface{}
			//log.Printf("DataType %v",colsTypes[i].DatabaseTypeName())
			switch colsTypes[i].DatabaseTypeName() {

			case "BIGINT":
				v, e = strconv.ParseInt(valores[i], 10, 64)
				if e != nil {
					return e
				}

			default:
				v = fmt.Sprintf("%s", valores[i])
			}
			registro[field] = v

		}
		res = append(res, registro)

	}
	*result = res
	return nil

}

func GetColumns() {

}
