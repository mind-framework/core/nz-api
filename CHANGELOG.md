## v1.0.0 (2021-05-10)

Changes:

New Features:

 - Se agrega archivo CHANGELOG.md, para notas de versionado utilizando vx.y.z utilizando
   x=version mayor, y=version menor y z=bugfix
