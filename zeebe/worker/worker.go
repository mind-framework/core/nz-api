//Package worker Paquete para interaccion con broker
package worker

import (
	"context"
	// "fmt"
	// "strconv"

	_log "github.com/rs/zerolog/log"
	"github.com/zeebe-io/zeebe/clients/go/pkg/entities"
	"github.com/zeebe-io/zeebe/clients/go/pkg/worker"
	"github.com/zeebe-io/zeebe/clients/go/pkg/zbc"
)

var log = _log.With().Str("ctx", "Worker").Logger()

const ResponseOk = 0
const ResponseError = 1
const ResponseFatal = 2

func init() {
}

//Worker estructura utilizada para interactuar con broker
type Worker struct {
	//Client Cliente zeebe para conexion con borker
	Client  zbc.Client
	handler func(*Jober) (int, error)
	//Type tipo de worker
	Type string
}

//NewWorker Establece la conexion con el borker y retorna un puntero a (zbc.Client,error)
func NewWorker(BrokerAddr string) (*Worker, error) {

	worker := &Worker{}

	// var err error
	Client, err := zbc.NewClient(&zbc.ClientConfig{
		GatewayAddress:         BrokerAddr,
		UsePlaintextConnection: true,
	})

	if err != nil {
		return nil, err
	}

	worker.Client = Client
	return worker, err
}

//Listen Escucha pecitiones del broker
func (w *Worker) Listen(nwcWorkerHandler func(*Jober) (int, error)) {

	log.Info().Msg("Iniciando Jober...")
	w.handler = nwcWorkerHandler
	jobWorker := w.Client.NewJobWorker().JobType(w.Type).Handler(w.HandleJob).Open()
	defer jobWorker.Close()

	jobWorker.AwaitClose()

}

//HandleJob Generico utlizado para todas las tareas
func (w *Worker) HandleJob(client worker.JobClient, job entities.Job) {

	var err error
	j := new(Job)
	log.Trace().Interface("JOB", job).Send()
	j.JobID = job.GetKey()
	j.ProcessID = job.WorkflowInstanceKey
	//Obtengo headers del job
	j.Headers, err = job.GetCustomHeadersAsMap()
	if err != nil {
		log.Error().Err(err).Msg("Error al obtener headers del Job")
		// failed to handle job as we require the custom job headers
		w.failJob(client, job)
		return
	}
	//Obtengo variables del job
	j.Variables, err = job.GetVariablesAsMap()
	if err != nil {
		log.Error().Err(err).Msg("Error al obtener variables del Job")
		// failed to handle job as we require the variables
		w.failJob(client, job)
		return
	}

	if v, ok := j.Variables["instanceID"].(float64); ok {
		j.InstanceID = int64(v)

	}

	if v, ok := j.Variables["taskName"].(string); ok && v != "" {

		j.TaskName = &v
	}
	var jober Jober
	var code int
	jober = j
	code, err = w.handler(&jober)
	if err != nil {
		switch code {
		case ResponseFatal:
			log.Error().Err(err).Msg("Error WorkerHandler")
			w.failJob(client, job)
			return
		case ResponseError:
			//Send Error
			log.Debug().Msg("Sending Message Error")
			return

		}

	}

	//Envio comando para completar el job
	request, err := client.NewCompleteJobCommand().JobKey(jober.GetJobID()).VariablesFromMap(jober.GetVariables())
	if err != nil {
		log.Error().Err(err).Msg("Error al completar job")
		w.failJob(client, job)
		return
	}
	ctx := context.Background()
	request.Send(ctx)
	// log.Info().Msg("Tarea completada")
	log.Trace().Interface("Sending Request", request).Send()

}

func (w *Worker) failJob(client worker.JobClient, job entities.Job) {
	ctx := context.Background()
	log.Error().Interface("Failed to complete job", job.GetKey()).Interface("Retries: ", job.Retries-1).Send()
	client.NewFailJobCommand().JobKey(job.GetKey()).Retries(job.Retries - 1).Send(ctx)
}

//Jober Interface para tipos de Jobs que reciben los workers
type Jober interface {
	GetJobID() int64
	GetInstanceID() int64
	GetProcessID() int64
	GetTaskName() *string
	GetHeaders() map[string]string
	GetVariables() map[string]interface{}
	SetVariables(map[string]interface{})
}

//Job Estructura de abstraccion de un job
type Job struct {
	JobID      int64
	ProcessID  int64
	InstanceID int64
	TaskName   *string
	Variables  map[string]interface{}
	Headers    map[string]string
}

//GetHeaders Retorna los headers del hob
func (j *Job) GetHeaders() map[string]string {
	return j.Headers
}

//GetInstanceID retorna el id de instancia de zeebe
func (j *Job) GetInstanceID() int64 {
	return j.InstanceID
}

//GetJobID retorna el id de instancia de zeebe
func (j *Job) GetJobID() int64 {
	return j.JobID
}

//GetProcessID Retorna el id del process en zeebe
func (j *Job) GetProcessID() int64 {
	return j.ProcessID
}

//GetTaskName Retorna el nombre de la tarea
func (j *Job) GetTaskName() *string {
	return j.TaskName
}

//GetVariables Retorna el nombre de la tarea
func (j *Job) GetVariables() map[string]interface{} {
	return j.Variables
}

//SetVariables Se agregan o modifican variables
func (j *Job) SetVariables(newMap map[string]interface{}) {
	for k, v := range newMap {
		j.Variables[k] = v
	}
	return
}
