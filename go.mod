module gitlab.com/mind-framework/core/nz-api

go 1.15

require (
	github.com/go-chi/chi v1.5.4
	github.com/go-chi/jwtauth v1.2.0
	github.com/jinzhu/gorm v1.9.16
	github.com/lestrrat-go/jwx v1.2.0
	github.com/unrolled/render v1.2.0
	golang.org/x/text v0.3.6
	gopkg.in/asn1-ber.v1 v1.0.0-20181015200546-f715ec2f112d // indirect
	gopkg.in/ldap.v2 v2.5.1
)
