//Package auth
package auth

import (
	"gitlab.com/mind-framework/admin-server/api.admin-server/api/authtype"
)

//AuthHandler Interface utilizada por los distintos modos de authentication
type AuthHandler interface {
	Login(*JWTModel, *authtype.ExtendedModel, string) (*AccountInfo, error)
	Connect(*authtype.ExtendedModel) error
	Disconnect()
}

//JWTModel Estructura en comun que devolveran todos package que implementen authentication
type JWTModel struct {
	Username   string
	Perfil     string
	Nombre     string
	Apellido   string
	SubGrupo   string
	CustomerID string
}

//AccountInfo Datos adicionales del usuario
type AccountInfo struct {
	CredentialsExpiresOn *string `json:"credentialsExpiresOn"`

	MustChangePassword *bool `json:"mustChangePassword"`
	PasswordExpiryNotification uint8 `passwordExpiryNotification`
}
