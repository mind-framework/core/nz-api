package ldap

import (
	"gitlab.com/mind-framework/admin-server/api.admin-server/api/authtype"
	"gitlab.com/mind-framework/core/nz-api/auth"
	// "gitlab.com/mind-framework/admin-server/api.admin-server/api/users"
)

type nwcLdap interface {
	Login(interface{}, *authtype.ExtendedModel, string) (*auth.JWTModel, error)
	Connect(*authtype.ExtendedModel) error
	Disconnect()
	AddRequest(interface{}) error
}
