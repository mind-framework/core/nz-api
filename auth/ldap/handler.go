//Package ldap
package ldap

import (
	"crypto/tls"
	"encoding/json"
	"errors"
	"fmt"
	_log "github.com/rs/zerolog/log"
	"strconv"
	"time"

	// "gitlab.com/newcom-lcs/admin-server/api.admin-server/api/authtype"
	"gitlab.com/mind-framework/admin-server/api.admin-server/api/authtype"
	"gitlab.com/mind-framework/core/nz-api/auth"
	"golang.org/x/text/encoding/unicode"
	"gopkg.in/ldap.v2"
)

var log = _log.With().Str("context", "LDAP").Logger()

//NewHandler retorna un auth.AuthHandler
func NewHandler() auth.AuthHandler {
	//cargo los modelos en el orms
	// model.Register(&Model{})

	h := &Ldap{}

	return h
}

//Ldap Estructura utilizada para conexiones ldap
type Ldap struct {
	Connection *ldap.Conn
	Config     map[string]interface{}
	Connected  bool
}

//NewLdap Retorna una instancia inicializada de la estructura ldap
func NewLdap() *Ldap {

	l := &Ldap{}
	l.Connected = false

	return l
}

//Connect Establece la una conexion ldap contra el servidor remoto. Se guarda en la estructura ldap
func (l *Ldap) Connect(at *authtype.ExtendedModel) error {
	var err error
	var js []byte



	// userModel := m.(map[string]interface{})
	log.Trace().Interface("LDAP Config", at.Configuracion).Msg("")
	// username := m.Username
	l.Config = make(map[string]interface{})

	js, err = json.Marshal(at.Configuracion)

	if err != nil {
		log.Error().Err(err).Msg("Error al leer configuracion")
	}

	json.Unmarshal(js, &l.Config)

	l.Connection, err = ldap.Dial("tcp", fmt.Sprintf("%s:%s", l.Config["servidor"], l.Config["puerto"]))

	if err != nil {
		log.Error().Err(err).Msg("Error de conexion ldap")
	}

	// Reconnect con TLS si esta habilitado
	if confTLS, ok := l.Config["tls"]; ok && confTLS.(bool) {

		err = l.Connection.StartTLS(&tls.Config{

			ServerName:         l.Config["dominio"].(string),
			InsecureSkipVerify: true,
		})
		if err != nil {
			log.Error().Err(err).Msg("Error al conectar con LDAP")
		}
	}

	if err == nil {
		l.Connected = true
		log.Debug().Interface("Servidor", l.Config["servidor"]).Msg("Conectado al servidor LDAP")
	}

	return err

}

//Disconnect Metodo utilizado para desconectarse del servidor remoto
func (l *Ldap) Disconnect() {
	log.Debug().Msg("Cerrando conexion Ldap")
	l.Connection.Close()
	return
}

//Bind que utiliza la conexion.
func (l *Ldap) Bind(username, password string) error {

	var (
		err       error
		bindParam string
		bindType  string
		ok        bool
	)

	if bindType, ok = l.Config["bindType"].(string); !ok {

		bindType = ""
	}

	switch bindType {

	case "userName":
		bindParam = username

	case "prependDomain":
		bindParam = fmt.Sprintf("%s\\%s", l.Config["dominio"], username)

	case "userLogonName":
		bindParam = fmt.Sprintf("%s@%s", username, l.Config["dominio"])

	default:
		bindParam = "cn=" + username + "," + l.Config["DN"].(string)
	}

	// First bind with a read only user
	err = l.Connection.Bind(bindParam, password)
	if err != nil {
		log.Error().Str("CAT","LDAP").Err(err).Msg("Error en Bind")

	}

	return err
}

//Login Metodo que valida usuario y contrasena del usuario
func (l *Ldap) Login(j *auth.JWTModel, at *authtype.ExtendedModel, password string) (*auth.AccountInfo, error) {

	username := j.Username

	var err error

	// First bind with a read only user
	err = l.Bind(username, password)
	if err != nil {
		return nil, err
	}

	var maxPwdAge int

	if value, ok := l.Config["maxPwdAge"]; ok && value != nil {
		maxPwdAge, err = strconv.Atoi(value.(string))
		if err != nil {
			return nil, errors.New("maxPwdAge is invalid")
		}
	}

	// if value, ok := l.Config["daysWarnPwd"]; ok && value != nil {
	// 	daysWarnPwd, err = strconv.Atoi(value.(string))
	// 	if err != nil {
	// 		return errors.New("daysWarnPwd is invalid")
	// 	}
	// }
	accountInfo := new(auth.AccountInfo)
	mustChangePassword := false
	accountInfo.MustChangePassword = &mustChangePassword
	//Si esta seteado los valores de caducidad de contraseña realizo la validacion
	if maxPwdAge != 0 {

		// Busco el usuario en ldap
		searchRequest := ldap.NewSearchRequest(
			fmt.Sprintf("%s", l.Config["DN"]),
			ldap.ScopeWholeSubtree, ldap.NeverDerefAliases, 0, 0, false,
			fmt.Sprintf("(&(objectClass=organizationalPerson)(cn=%s))", j.Username),
			[]string{"pwdLastSet"},
			nil,
		)
		var s *ldap.SearchResult
		s, err = l.Connection.Search(searchRequest)
		if err != nil {
			log.Error().Str("CAT","LDAP").Err(err).Msg("Error en Search")
			return nil, err
		}
		log.Trace().Str("CAT","LDAP").Interface("Entries",s.Entries).Msg("")

		//Verifico si encontre 1 y verifico la fecha
		if len(s.Entries) == 1 {

			dateInt, _ := strconv.ParseInt(s.Entries[0].GetAttributeValue("pwdLastSet"), 10, 64)
			pwdLastSet := l.parseDateInteger(dateInt)
			pwdExpireOn := pwdLastSet.AddDate(0, 0, maxPwdAge)

			// now := time.Now()

			// daysLastSetPwd := int(now.Sub(pwdLastSet).Hours() / 24)

			log.Trace().Str("CAT","LDAP").Str("pwdLastSet",pwdLastSet.String()).Msg("Fecha de expiracion de password")
			log.Trace().Str("CAT","LDAP").Str("daysLastSetPwd",pwdLastSet.String()).Msg("Dias desde el utlimo cambio de clave")

			// minDays := maxPwdAge - daysWarnPwd
			// remainingDaysPwd := (maxPwdAge - daysLastSetPwd)

			// if minDays >= daysLastSetPwd {
			// 	err = errors.New(fmt.Errorf("[warning] La clave caduca en %s dia/s", remainingDays))
			// }
			parsePwdExpireOn := fmt.Sprintf("%d-%02d-%02d", pwdExpireOn.Year(), pwdExpireOn.Month(), pwdExpireOn.Day())
			accountInfo.CredentialsExpiresOn = &parsePwdExpireOn

		}
	}

	return accountInfo, err
}

//ResultSearch es tipo que ereda de ldap.SearchResult
type ResultSearch struct {
	ldap.SearchResult
}

//Search Metodo para realizar busquedas en LDAP
func (l *Ldap) Search(Filter string, Attributes []string) (searchResult *ldap.SearchResult, err error) {

	// Search for the given username
	searchRequest := ldap.NewSearchRequest(
		fmt.Sprintf("%s", l.Config["DN"]),
		ldap.ScopeWholeSubtree, ldap.NeverDerefAliases, 0, 0, false,
		Filter,
		Attributes,
		nil,
	)

	searchResult, err = l.Connection.Search(searchRequest)
	if err != nil {
		log.Error().Err(err).Msg("Error en Search")
		return nil, err
	}

	return searchResult, err

}

//parseDateInteger Funcion local para convertir fechas integer de ldap
func (l *Ldap) parseDateInteger(dateInt int64) time.Time {

	dateInt = dateInt / 10000000
	winSec := ((1970 - 1601) * 365.242190) * 86400
	dateInt = dateInt - int64(winSec)
	tm := time.Unix(dateInt, 0)
	return tm

}

//PasswordReset Resetea la clave de un usuario
func (l *Ldap) PasswordReset(userName, oldpassword, newPassword string) (err error) {

	// passwordModifyRequest := ldap.NewPasswordModifyRequest("cn="+userName+","+l.Config["DN"].(string), "", newPassword)
	// log.Printf("USERIDENTITY= %v", passwordModifyRequest.UserIdentity)
	// _, err = l.Connection.PasswordModify(passwordModifyRequest)

	// if err != nil {
	// 	log.Printf("Password could not be changed: %v", err.Error())
	// }

	utf16 := unicode.UTF16(unicode.LittleEndian, unicode.IgnoreBOM)
	encoded, err := utf16.NewEncoder().String("\"" + newPassword + "\"")

	modify := ldap.NewModifyRequest("cn=" + userName + "," + l.Config["DN"].(string))
	modify.Replace("unicodePwd", []string{encoded})

	err = l.Connection.Modify(modify)

	if err != nil {
		log.Error().Err(err).Msg("Error al intentar modificar contraseña")
	}

	return err
}

//SetUserStatus Metodo para deshabilitar usuario ldap
//@status - "Habilitado","Deshabilitado"
func (l *Ldap) SetUserStatus(userName, status string) error {
	var err error
	var result *ldap.SearchResult

	const DisableChar = 2

	result, err = l.Search(fmt.Sprintf("(&(objectCategory=person)(objectClass=user)(cn=%s))", userName), []string{"DN"})
	if err != nil {
		log.Error().Err(err).Msg("Error al intentar cambiar estado de usuario")
	}
	log.Debug().Str("status",status).Msg("Modificando estado de usuario")

	if result.Entries != nil && len((*result).Entries) == 1 {
		log.Debug().Str("username", userName).Msg("Usuario encontrado")
		result.Entries[0].Print()
		uac := string(result.Entries[0].GetAttributeValue("userAccountControl"))
		if uac == "" {
			uac = "512"
		}
		//! comentado por mnunez 2021-02-11
		// userAC := 0
		// userAC, err := strconv.Atoi(uac)

		if err != nil {
			return err
		}

		modify := ldap.NewModifyRequest("cn=" + userName + "," + l.Config["DN"].(string))

		//Valido el estado y lo seteo utilizando bitwise
		switch status {
		case "Deshabilitado":
			//! comentado por mnunez 2021-02-11
			// b := userAC | DisableChar
			// log.Printf("Seteando %#v", strconv.Itoa(b))
			// modify.Replace("userAccountControl", []string{strconv.Itoa(b)})
			modify.Replace("userAccountControl", []string{"514"})

		case "Habilitado":
			//! comentado por mnunez 2021-02-11
			// b := userAC &^ DisableChar
			// log.Printf("Seteando %v", b)
			modify.Replace("userAccountControl", []string{"512"})
		default:
			return errors.New("parametro <estado> invalido")
		}
		log.Trace().Interface("ReplaceAttributes",modify.ReplaceAttributes).Msg("")
		err = l.Connection.Modify(modify)

		if err != nil {
			log.Error().Err(err).Msg("Error LDAP Modify Status")
		}
	}

	return err
}

//UpdateUser Modificacion de atributos de usuario
func (l *Ldap) UpdateUser(data map[string]interface{}, userName string) error {

	var (
		err error
	)

	var result *ldap.SearchResult
	query := fmt.Sprintf("(&(objectCategory=person)(objectClass=user)(name=%s))", userName)
	result, err = l.Search(query, []string{"DN"})
	log.Trace().Interface("result",result).Send()
	if err != nil {
		return err
	}

	if result == nil && len(result.Entries) != 1 {

		return errors.New("user not found")

	}
	modify := ldap.NewModifyRequest("cn=" + userName + "," + l.Config["DN"].(string))

	oldp := ""
	if data["oldPassword"] != nil {
		oldp = data["oldPassword"].(string)
	}
	//Verifico Password

	if data["password"] != nil {
		log.Debug().Msg("Reseteando password")
		err = l.PasswordReset(userName, oldp, data["password"].(string))
		if err != nil {
			return err
		}
		log.Debug().Msg("Habilitando Usuario")
		l.SetUserStatus(userName, "Habilitado")
	}

	if data["estado"] != nil {
		log.Debug().Str("estado",data["estado"].(string)).Msg("Cambiando estado de usuario")
		l.SetUserStatus(userName, data["estado"].(string))
	}

	//Actualizo atributos genericos
	log.Printf("Actualizando atributos varios")
	bindAttributes := l.Config["bindAttributes"].(map[string]interface{})

	log.Debug().Msg("Actualizando atributos LDAP")
	for k, v := range bindAttributes {
		if data[v.(string)] != nil {
			value := data[v.(string)].(string)
			log.Trace().Str(v.(string), value).Msg("")
			modify.Replace(k, []string{value})
		}

	}
	if len((*modify).ReplaceAttributes) > 0 {
		err = l.Connection.Modify(modify)
		if err != nil {
			log.Error().Err(err).Msg("Error a actualizar usuario")
		}
	}
	return err
}

//AddUser Metodo para realizar request ldap
func (l *Ldap) AddUser(m map[string]interface{}) error {

	var (
		err error
	)
	bindAttributes := l.Config["bindAttributes"].(map[string]interface{})

	userName := m["username"].(string)
	userPassword := m["password"].(string)

	var userAC string
	if estado, ok := m["estado"].(string); ok {
		if estado == "Habilitado" {
			userAC = "512"
		} else {
			userAC = "514"
		}
	}

	//ATRIBUTOS BASE LDAP
	a := ldap.NewAddRequest("cn=" + userName + "," + l.Config["DN"].(string))
	a.Attribute("objectClass", []string{"person", "organizationalPerson", "user", "top"})
	//UserName
	a.Attribute("uid", []string{userName})
	//UserName
	a.Attribute("cn", []string{userName})
	//Password
	utf16 := unicode.UTF16(unicode.LittleEndian, unicode.IgnoreBOM)
	encoded, err := utf16.NewEncoder().String("\"" + userPassword + "\"")
	a.Attribute("unicodePwd", []string{encoded})

	//Logon Name
	// a.Attribute("userPrincipalName", []string{"readUser1@newcom-lcs.com"})
	a.Attribute("userPrincipalName", []string{userName + "@" + l.Config["dominio"].(string)})
	//Last Name
	//Logon Name PreWindows
	a.Attribute("sAMAccountName", []string{userName})

	//ACCOUNTDISABLE
	a.Attribute("userAccountControl", []string{userAC})

	//Debe cambiar la clave en el proximo login
	// "0" will user to force "password change on next login"
	// "-1" will user password no expire
	a.Attribute("pwdLastSet", []string{"-1"})

	//ATRIBUTOS ADICIONALES
	//Recorro atributos para bind
	for k, v := range bindAttributes {

		if value, ok := m[v.(string)]; ok {
			a.Attribute(k, []string{value.(string)})
		}

	}

	err = l.Connection.Add(a)

	if err != nil {
		return err
	}

	return err
}

//DelUser Metodo para realizar request ldap
func (l *Ldap) DelUser(userName string) (err error) {

	log.Debug().Str("username", userName).Msg("Eliminando usuario")

	if userName == "" {

		return errors.New("El usuario a eliminar no puede estar vacio")

	}

	delRequest := ldap.NewDelRequest("cn="+userName+","+l.Config["DN"].(string), nil)
	err = l.Connection.Del(delRequest)
	return err

}

// func(l Ldap)mapAttributes(attributeMap map[string]string, *ldap.AddRequest)
