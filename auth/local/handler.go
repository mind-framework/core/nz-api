//Package auth
package local

import (

	// "gitlab.com/newcom-lcs/core/nwc-api/auth"

	"encoding/json"
	"log"
	"time"

	"gitlab.com/mind-framework/admin-server/api.admin-server/api/authtype"
	"gitlab.com/mind-framework/admin-server/api.admin-server/api/users"
	"gitlab.com/mind-framework/core/nz-api/auth"
	"gitlab.com/mind-framework/core/nz-api/model"
)

//New
func NewHandler() auth.AuthHandler {
	//cargo los modelos en el orm
	// model.Register(&Model{})

	h := &Local{}
	return h
}

//Local Estructura para conexiones locales
type Local struct{}

//Connect Establece la una conexion ldap contra el servidor remoto. Se guarda en la estructura ldap
func (l *Local) Connect(at *authtype.ExtendedModel) error {

	return nil

}

//Disconnect Metodo utilizado para desconectarse del servidor remoto
func (l *Local) Disconnect() {

}

//Login Metodo para validar usuario y contrasena
func (l *Local) Login(j *auth.JWTModel, at *authtype.ExtendedModel, password string) (*auth.AccountInfo, error) {
	log.Printf("AT: %#v", (*at).SecurityPolicy)

	var err error
	modelo := &users.ExtendedModel{}
	scope := model.ORM.Set("gorm:auto_preload", true).Limit(1)
	scope.Where("username = ?", j.Username).Find(modelo)
	err = modelo.VerifyPassword(password)
	if err != nil {
		return nil, err
	}
	ai := new(auth.AccountInfo)
	var datePasswordChanged time.Time
	if ai.MustChangePassword == nil {
		cambiarPass := false
		ai.MustChangePassword = &cambiarPass
	}
	if modelo.LastChangedPassword != nil {
		d, er := time.Parse(time.RFC3339, *modelo.LastChangedPassword)
		if er != nil {
			*ai.MustChangePassword = true
			// return nil, errors.New("Error al validar fecha de expiración de contraseña")
		} else {

			datePasswordChanged = d
		}
	} else {
		datePasswordChanged = time.Now()
		*ai.MustChangePassword = true
	}

	securityPolicy := make(map[string]interface{})
	var js []byte
	js, err = json.Marshal(at.SecurityPolicy)
	if err != nil {
		log.Printf("Error al leer configuracion...")
	}
	json.Unmarshal(js, &securityPolicy)

	log.Printf("SP: %#v", securityPolicy)
	if v, ok := securityPolicy["passwordExpiryNotification"]; ok {

		ai.PasswordExpiryNotification = uint8(v.(float64))
	}

	var datePassword *time.Time
	if v, ok := securityPolicy["passwordExpiry"]; ok {
		passwordExpiryDays := int(v.(float64))

		if passwordExpiryDays == 0 {
			log.Printf("0")
			datePassword = nil
		} else {
			t := datePasswordChanged.AddDate(0, 0, passwordExpiryDays)
			datePassword = &t
			log.Printf(">0")
		}

	} else {
		datePassword = nil
	}

	if datePassword != nil {

		date := datePassword.Format("2006-01-02")
		ai.CredentialsExpiresOn = &date

	} else {
		ai.CredentialsExpiresOn = nil
	}
	if !*ai.MustChangePassword {
		ai.MustChangePassword = modelo.ChangePassword
	}

	return ai, err
}
