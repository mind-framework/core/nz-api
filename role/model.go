//Package role project role.go
package role

import (
	"errors"
	"log"

	"gitlab.com/mind-framework/core/nz-api/filter"
	"gitlab.com/mind-framework/core/nz-api/model"
	"github.com/jinzhu/gorm"
	//"golang.org/x/crypto/bcrypt"
)

//ErrNotFound Declara mensaje para perfil no encontrado
var (
	ErrNotFound  = errors.New("No se encontró el perfil especificado")
	pks          []string
	tableName    string
	validFilters []filter.ValidFilter
)

func init() {
	log.Println("inicializando model Role")
	pks = append(pks, "nombre")
	tableName = "profiles"

}

//Init Seteo de configuraciones en base de datos
func (u *Model) Init() {

	return
}

//Model_Profiles campos basico de la tabla
type Model struct {
	Nombre      string  `json:"nombre" gorm:"primary_key;not null;unique"`
	Descripcion *string `json:"descripcion,omitempty" `
	ExtendedFields
	//	Permisos *UriMap `json:"permisos" gorm:"type:text"`
}

//ExtendedFields Campos Especiales del recurso
type ExtendedFields struct {
	Permisos *Role `json:"permisos" gorm:"type:text"`
}

//Fields ---
func (u *Model) Fields() *Model {
	return u
}

//Keys devuelve las claves primarias del modelo
func (u *Model) Keys() []string {
	return pks
}

//TableName devuelve el nombre de la tabla asociada al modelo
func (Model) TableName() string {
	return tableName
}

//NewModel Retorna un Modeler.Model del modelo
func (u Model) NewModel(value map[string][]string, extended bool) model.Modeler {
	m := &Model{}
	m.SetPkValue(value)
	return m
}

//NewModelList Devuelve una slice de modelos
func (u Model) NewModelList(extended bool) interface{} {
	m := new([]Model)
	return m
}

//SetPkValue Setea el pk de la tabla
func (u *Model) SetPkValue(value map[string][]string) error {

	if len(value["id"]) > 0 {

		u.Nombre = value["id"][0]
	} else {
		u.Nombre = ""
	}
	return nil
}

//ValidFilters Retorna un slice de filtros aceptados
func (u *Model) ValidFilters() []filter.ValidFilter {

	return validFilters
}

//BeforeSave Hook que se ejecuta antes de salvar enviar un registro a la BD
//  El hook se ejecuta tanto en los updates como en los inserts
// 	Para modificar un registro se debe utilizar:
//     scope.SetColumn("campo", vapor)
func (u *Model) BeforeSave(scope *gorm.Scope) (err error) {

	return nil
}

//AfterSave Hook que se ejecuta luego de salvar los datos en la BD
//  El hook se ejecuta tanto en los updates como en los inserts
func (u *Model) AfterSave(scope *gorm.Scope) error {

	//	if err := u.VerifyPassword(u.ClearPassword); err != nil {
	//		return err
	//	}
	//	u.ClearPassword = "****"
	return nil
}

//BeforeCreate Hook que se ejecuta antes de insertar un registro
func (u *Model) BeforeCreate(this *gorm.Scope) error {
	// switch {
	// case u.PasswordPublico == "" || u.PasswordPublico == "****":
	// 	return errors.New("Valor inválido para la contraseña")
	// }

	// pass, err := u.generarPassword(u.PasswordPublico)
	// if err != nil {
	// 	return err
	// }
	// this.SetColumn("password", pass)
	return nil
}

//AfterCreate Hook que se ejecuta luego de crear un registro
func (u *Model) AfterCreate(tx *gorm.DB) error {
	//	u.PasswordPublico = "****"
	return nil
}

//BeforeUpdate Hook que se ejecuta antes de actualizar un registro
func (u *Model) BeforeUpdate(scope *gorm.Scope) error {
	// log.Println("BeforeUpdate")
	// switch {
	// case u.PasswordSeguro != "" && u.PasswordSeguro != "****":
	// 	pass, err := u.generarPassword(u.PasswordSeguro)
	// 	if err != nil {
	// 		return err
	// 	}
	// 	scope.SetColumn("password", pass)
	// }

	return nil
}

//AfterUpdate Hook que se ejecuta luego de actualizar un registro
func (u *Model) AfterUpdate(tx *gorm.DB) error {

	return nil
}

//BeforeDelete Se ejecuta antes de elminar un registro de la BD
// Si el método retorna un error, se cancela la eliminación
func (u *Model) BeforeDelete() error {
	return nil
}

//AfterDelete Se ejecuta antes de elminar un registro de la BD
// Si el método retorna un error, se cancela la eliminación
func (u *Model) AfterDelete(tx *gorm.DB) error {
	return nil
}

//AfterFind Se ejecuta luego de recibir el registro desde la bd
func (u *Model) AfterFind() (err error) {

	return
}

//generarPassword Genera un hash del password para almacenarlo en la BD
func (u *Model) generarPassword(str string) (string, error) {

	return "", nil
}

//VerifyPassword Verifica el password con el Hash del password del modelo
func (u *Model) VerifyPassword(pass string) error {

	//m = Model()
	return nil
}

//GetError Devuelve una descripción del error
func (u *Model) GetError(code int, err error) (int, error) {
	var e = err.Error()
	switch {
	case code == 404:
		e = "El perfil especificado no existe"

	}
	return code, errors.New(e)
}

//MemoryLoad Cargo en memoria la informacion de los perfiles
// func (u *Model) MemoryLoad() error {

// 	// }

// 	return nil
// }
