//Package role handlers para Perfiles de usuarios y sus permisos asociados
package role

import (
	"log"

	"gitlab.com/mind-framework/core/nz-api/model"
)

func init() {

}

//CargarDatos obtiene los perfiles y los devuelve en formato Role
func CargarDatos(moduleName string) (Role, error) {

	list := new([]Model)
	// log.Printf("Module Name: %v\n\n", moduleName)

	scope := model.ORM

	scope.Find(list)
	r := make(Role)

	for _, u := range *list {
		//	log.Printf("Perfil: %v", u.Nombre)

		for k, b := range *u.Permisos {
			if k == moduleName {
				//var r Role

				// var uri Uri
				uri := make(Uri)
				//				var perm Permiso

				for i, p := range b {

					uri[i] = p

					// log.Printf("Uri: %v", i)
					// log.Printf("Permiso: %v", p)

				}

				r[u.Nombre] = uri

			}
		}
		log.Printf("----------")
	}
	// log.Printf("Role: %v", r)
	return r, nil
}
