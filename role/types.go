//Package role asd
package role

import (
	"database/sql/driver"
	"encoding/json"
)

const (
	P_READ      = 1
	P_WRITE     = 2
	P_APPEND    = 4
	P_DELETE    = 8
	P_ESPECIAL1 = 16
	P_ESPECIAL2 = 32
)

type Permiso struct {
	READ   bool `json:"read"`
	WRITE  bool `json:"write"`
	APPEND bool `json:"append"`
	DELETE bool `json:"delete"`
	ESP1   bool `json:"esp1"`
	ESP2   bool `json:"esp2"`
}

//Uri tipo de dato map[string]Permiso
type Uri map[string]Permiso

//UriMap tipo de dato donde la raiz es un profile
type UriMap map[string]Uri

//UriMap tipo de dato donde la raiz es un Modulo
type Role map[string]Uri

//Scan - devuelve un array de un objeto json
func (r *Role) Scan(value interface{}) error {

	if b, ok := value.([]byte); ok == true {

		json.Unmarshal(b, &r)
	} else {

		var m []byte
		var err2 error

		if m, err2 = json.Marshal(value); err2 == nil {
			json.Unmarshal(m, &r)

		}

	}

	return nil
}

//Value - devuelve la versión json de la variable
func (r *Role) Value() (driver.Value, error) {

	if r.IsNull() {
		return nil, nil
	}

	return json.Marshal(r)
}

//IsNull Verifica si UriMap es null
func (r *Role) IsNull() bool {

	if len(map[string]Uri(*r)) == 0 {
		return true
	}
	return false

	return false

}

//Tipo struct para guardar permisos
type Permisos struct {
	Perfiles_C  uint64 `json:"perfilesC"`
	Perfiles_R  uint64 `json:"perfilesR"`
	Perfiles_U  uint64 `json:"perfilesU"`
	Perfiles_D  uint64 `json:"perfilesD"`
	Perfiles_E1 uint64 `json:"perfilesE1"`
	Perfiles_E2 uint64 `json:"perfilesE2"`
}

//Tipo de dato para guardar permisos de un recurso
type Recurso map[string]Permisos

//Tipo de dato para guardar modulos con recursos y permisos
type Modulo map[string]Recurso

func (r *Modulo) Scan(value interface{}) error {

	if b, ok := value.([]byte); ok == true {

		json.Unmarshal(b, &r)
	} else {

		var m []byte
		var err2 error

		if m, err2 = json.Marshal(value); err2 == nil {
			json.Unmarshal(m, &r)

		}

	}

	return nil
}

//Value - devuelve la versión json de la variable
func (r *Modulo) Value() (driver.Value, error) {

	if r.IsNull() {
		return nil, nil
	}

	return json.Marshal(r)
}

//IsNull Verifica si UriMap es null
func (r *Modulo) IsNull() bool {

	if len(map[string]Recurso(*r)) == 0 {
		return true
	}
	return false

	return false

}

type Roles struct {
	Recursos Modulo
}

// func (r *Role) CargarDatos() (Role, error) {

// 	list := new([]Model)

// 	scope := model.ORM
// 	scope.Find(list)
// 	fmt.Printf("List: %+v\n", list)
// 	// if app.Role == nil {

// 	// 	app.Role = *list
// 	// 	log.Printf("Global Permisos: %v", Perm)
// 	// }

// 	// for _, u := range *list {
// 	// 	log.Printf("U: %v", u)
// 	// 	log.Printf("Nombre: %v", u.Nombre)
// 	// 	//log.Printf("Uri: ", u.Uris)
// 	// 	for _, b := range *u.Permisos {
// 	// 		log.Printf("B: %v", b)

// 	// 		for i, p := range b {

// 	// 			// 	// for _, p := range b["Uri"] {
// 	// 			log.Printf("Indice: %v", i)
// 	// 			log.Printf("Permiso: %v", p)

// 	// 		}
// 	// 	}
// 	// }

// 	return nil, nil
// }

// func (p *UriMap) fromSlice(list interface{}) error {

// 	fmt.Printf("list: %+v\n", list)

// 	return nil
// }

// type Permisos []Uri

/*
func (p *Permiso) Value() (driver.Value, error) {
	log.Printf("Permiso Value: %+v\n", p)
	if p.IsNull() {
		return nil, nil
	}

	return json.Marshal(p)
}

func (p Permiso) IsNull() bool {

	if p == 0 {
		return true
	}
	return false
}

func (p *Permiso) Scan(value interface{}) error {
	log.Printf("Permiso Scan: %v\n", p)

	if b, ok := value.([]byte); ok == true {

		json.Unmarshal(b, &p)
	} else {
		//p.fromSlice(value)
		log.Printf("Scan FromSlice")
		//		m,_ := json.Marshal(value)
		//		json.Unmarshal(m, j)
	}

	return nil
}

func (p Permiso) MarshalJSON() ([]byte, error) {

	log.Printf("MarshalJSON")
	var permiso = map[string]bool{
		"READ":   false,
		"WRITE":  false,
		"APPEND": false,
		"DELETE": false,
		"ESP1":   false,
		"ESP2":   false,
	}

	if (p & P_READ) != 0 {
		permiso["READ"] = true
	}
	if (p & P_WRITE) != 0 {
		permiso["WRITE"] = true
	}
	if (p & P_APPEND) != 0 {
		permiso["APPEND"] = true
	}
	if (p & P_DELETE) != 0 {
		permiso["DELETE"] = true
	}
	if (p & P_ESPECIAL1) != 0 {
		permiso["ESP1"] = true
	}
	if (p & P_ESPECIAL2) != 0 {
		permiso["ESP2"] = true
	}

	return json.Marshal(permiso)

}
*/
/*
//UnmarshalJSON método que realiza la conversión desde JSON
func (p *Permisos) UnmarshalJSON(data []byte) error {
	log.Printf("Unmarshal")
	var list interface{}

	//	*p = 0

	if err := json.Unmarshal(data, &list); err == nil {
		log.Printf("list= %v", list)

		p.fromSlice(list)
		//	log.Printf("FROMSLICE: %#v", string(data[0]))
		log.Printf("p= %v", p)
	}

	return nil
}

func (p *Permisos) fromSlice(list interface{}) error {
	log.Printf("fromMegaSLICEINTERFACEARRAY")
	var per = Permiso{false, false, false, false, false, false}
	var uri Uri
	var arr []Uri
	for _, z := range list.([]interface{}) {
		log.Printf("1")
		for i, b := range z.(map[string]interface{}) {

			if i == "permiso" {
				log.Printf("2")
				for n, v := range b.(map[string]interface{}) {
					log.Printf("2")
					if string(n) == "read" {
						if v.(bool) == true {
							per.READ = true
						}
					}
					if string(n) == "write" {
						if v.(bool) == true {
							per.WRITE = true
						}
					}
					if string(n) == "append" {
						if v.(bool) == true {
							per.APPEND = true
						}
					}
					if string(n) == "delete" {
						if v.(bool) == true {
							per.DELETE = true
						}
					}
					if string(n) == "esp1" {
						if v.(bool) == true {
							per.ESP1 = true
						}
					}
					if string(n) == "esp2" {
						if v.(bool) == true {
							per.ESP2 = true
						}
					}

				}
				if i == "modulo" {
					uri.Modulo = b.(string)
				}
				if i == "uri" {
					uri.Uri = b.(string)
				}

			}

		}
		log.Printf("Uri: %#v", uri)
		arr = append(arr, uri)
	}
	*p = arr
	//p = append(p, uri)

	return nil
}

// 	for i, b := range list.(map[string]interface{}) {
// 		if i=="permiso" {

// 			for j, b := range list.(map[string]interface{}){
// 				p[i].
// 			}

// 		}
// 	}

// }
/*
func (p *Permiso) fromSlice(list interface{}) error {
	log.Printf("fromSlice")
	*p = 0

	//log.Printf("MAP: %#v", list.([]interface{}))
	log.Printf("fromSlice")
	for i, b := range list.(map[string]interface{}) {

		if i == "READ" {
			if b.(bool) == true {
				*p += Permiso(P_READ)
			}
		}
		if i == "WRITE" {
			if b.(bool) == true {
				*p += Permiso(P_WRITE)
			}
		}
		if i == "APPEND" {
			if b.(bool) == true {
				*p += Permiso(P_APPEND)
			}
		}
		if i == "DELETE" {
			if b.(bool) == true {
				*p += Permiso(P_DELETE)
			}
		}
		if i == "ESP1" {
			if b.(bool) == true {
				*p += Permiso(P_ESPECIAL1)
			}
		}
		if i == "ESP2" {
			if b.(bool) == true {
				*p += Permiso(P_ESPECIAL2)
			}
		}

		//	log.Printf("i: %v", i)
		//	log.Printf("b: %v", b)
	}
	log.Printf("P: %v", p)
	return nil
}
*/
/*
func (p *Permisos) Value() (driver.Value, error) {
	log.Printf("Value")
	if p.IsNull() {
		return nil, nil
	}

	return json.Marshal(p)
}

// //Scan - lee los días de la seman desde un json o de un array
func (p *Permisos) Scan(value interface{}) error {
	log.Printf("Scan")

	if b, ok := value.([]byte); ok == true {
		json.Unmarshal(b, &p)
		log.Printf("1")
	} else {
		var m []byte
		var err2 error
		log.Printf("2")
		if m, err2 = json.Marshal(value); err2 == nil {
			json.Unmarshal(m, &p)
			log.Printf("3")
		} else {
			log.Printf("Error: %v", err2)
			log.Printf("4")
		}

	}

	return nil
}

// //MarshalJSON Devuelve una representación JSON de los dias de la semana
// func (wd WeekDays) MarshalJSON() ([]byte, error) {
// 	var i []string
// 	if wd.Mon {
// 		i = append(i, "Lun")
// 	}
// 	if wd.Tue {
// 		i = append(i, "Mar")
// 	}
// 	if wd.Wed {
// 		i = append(i, "Mié")
// 	}
// 	if wd.Thu {
// 		i = append(i, "Jue")
// 	}
// 	if wd.Fri {
// 		i = append(i, "Vie")
// 	}
// 	if wd.Sat {
// 		i = append(i, "Sáb")
// 	}
// 	if wd.Sun {
// 		i = append(i, "Dom")
// 	}

// 	return json.Marshal(i)

// }

// //UnmarshalJSON método que realiza la conversión desde JSON
// func (wd *WeekDays) UnmarshalJSON(data []byte) error {
// 	var list interface{}

// 	*wd = WeekDays{false, false, false, false, false, false, false}

// 	if err := json.Unmarshal(data, &list); err == nil {
// 		wd.fromSlice(list)

// 	}

// 	return nil
// }

// func (p Permiso) IsNull() bool {
// 	return !p.READ && !p.WRITE && !p.APPEND && !p.DELETE && !p.ESP1 && !p.ESP2
// }

func (p Permisos) IsNull() bool {
	if len(p) == 0 {
		return true
	}
	return false

}

// func (p *Permisos)

// {
// 	"nombre":"Administrador",
// 	"descripcion":"Administradores del sitio web"
// 	"permisos":[
// 		{
// 			"modulo":"admin",
// 			"uri":"/users",
// 			"permiso":"8"
// 		},
// 		{
// 			"modulo":"admin",
// 			"uri":"/modules",
// 			"permiso":"5"
// 		}
// 		{
// 			"modulo":"reportes",
// 			"uri":"/registros",
// 			"permiso":"1"
// 		}
// 	]

// }

// {
// 	"nombre":"Administrador",
// 	"descripcion":"Administradores del sitio web"
// 	"permisos":[
// 		{
// 			"admin:/users": {
// 				"READ":"false",
// 				"WRITE":"false",
// 				"APPEND":"false",
// 				"DELETE":"true",
// 				"ESP1":"false",
// 				"ESP2":"false",
// 			}
// 		},
// {
// 	"modulo":"admin",
// 	"uri":"/modules",
// 	"p":{
// 		"READ":"true",
// 		"WRITE":"true",
// 		"APPEND":"false",
// 		"DELETE":"false",
// 		"ESP1":"false",
// 		"ESP2":"false",
// 	}
// }
// 		{
// 			"modulo":"reportes",
// 			"uri":"/registros",
// 			"READ":"false",
// 			"WRITE":"true",
// 			"APPEND":"false",
// 			"DELETE":"false",
// 			"ESP1":"false",
// 			"ESP2":"false",
// 		}
// 	]

// }

type UriMap map[string]Permiso

func (u *UriMap) Value() (driver.Value, error) {
	log.Printf("Value")
	if u.IsNull() {
		return nil, nil
	}

	return json.Marshal(u)
}

// //Scan - lee los días de la seman desde un json o de un array
func (u *UriMap) Scan(value interface{}) error {
	log.Printf("Scan")

	if b, ok := value.([]byte); ok == true {
		json.Unmarshal(b, &u)
		log.Printf("1")
	} else {
		var m []byte
		var err2 error
		log.Printf("2")
		if m, err2 = json.Marshal(value); err2 == nil {
			json.Unmarshal(m, &u)
			log.Printf("3")
		} else {
			log.Printf("Error: %v", err2)
			log.Printf("4")
		}

	}

	return nil
}

func (u UriMap) IsNull() bool {
	if len(u) == 0 {
		return true
	}
	return false

}
*/
