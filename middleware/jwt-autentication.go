package middleware

import (
	"context"

	_log "github.com/rs/zerolog/log"

	// "log"
	"net/http"
	"os"
	"strconv"

	"encoding/json"
	"io/ioutil"

	"github.com/go-chi/jwtauth"
	// "github.com/lestrrat-go/jwx/jwt"
	"gitlab.com/mind-framework/core/nz-api/app"
)

var log = _log.With().Str("pkg", "middleware").Logger()

//var AuthServerUrl string

// AuthErrorHandler Middleware que verifica el token de autenticación, enviando un mensaje de error compatible
// 	con el Framework Si se recibe un parámetro errorHandler, se deriva a la funcion en caso de error
func AuthErrorHandler(errorHandler http.HandlerFunc) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			var log = log.With().Str("ctx", "AuthErrorHandler").Logger()

			params := ContextParams(r)

			token, claims, _ := jwtauth.FromContext(r.Context())
			log.Trace().Interface("token", token).Msg("")

			if token == nil {
				// if token != nil {
				// 	log.Printf("Validacion Token: ",)
				// }
				if errorHandler != nil {
					errorHandler.ServeHTTP(w, r)
					return
				}

				app.Fail(w, http.StatusText(401), 401)
				return
			}

			sub, ok := claims["sub"].(string)
			if !ok || sub == "" {
				if errorHandler != nil {
					errorHandler.ServeHTTP(w, r)
					return
				}
				app.Error(w, "Token inválido (sub)")
				return
			}

			rol, ok := claims["rol"].(string)
			if !ok || rol == "" {
				if errorHandler != nil {
					errorHandler.ServeHTTP(w, r)
					return
				}
				app.Error(w, "Token inválido (rol)")
				return
			}

			cid, _ := claims["cid"].(string)

			fn, _ := claims["fn"].(string)

			ln, _ := claims["ln"].(string)

			grp, _ := claims["grp"].(string)
			params.UserInfo.Apellido = ln
			params.UserInfo.Nombre = fn
			params.UserInfo.UserName = sub
			params.UserInfo.Rol = rol
			params.UserInfo.Grupo = grp
			params.UserInfo.Cliente = cid
			ctx := context.WithValue(r.Context(), "RequestParams", params)

			TokenAuth := jwtauth.New("HS256", []byte(app.Secret), nil)
			_, tokenString, _ := TokenAuth.Encode(claims)
			var secureCookie bool
			if sc, err := strconv.ParseBool(os.Getenv("API_COOKIE_SECURE")); err == nil {
				secureCookie = sc

			}
			c := &http.Cookie{
				Name:     "jwt",
				Secure:   secureCookie,
				HttpOnly: true,
				Path:     "/",
				Value:    tokenString,
				Domain:   os.Getenv("API_TOKEN_DOMAIN"),
			}
			//Sete la cookie en la respuesta
			http.SetCookie(w, c)

			next.ServeHTTP(w, r.WithContext(ctx))

		})
	}
}

//ReadCookieFromJson Lee los datos de cookie desde archivo
func ReadCookieFromJson(errorHandler http.HandlerFunc) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

			params := ContextParams(r)
			log.Printf("Cargando cookie desde Archivo json...")

			cookieJson, err := os.Open("standalone-dev/cookie.json")
			if err != nil {
				log.Printf("Error al leer cookie %v: ", err)
				return
			}

			defer cookieJson.Close()
			cookieValue, _ := ioutil.ReadAll(cookieJson)
			var cookie map[string]string
			json.Unmarshal([]byte(cookieValue), &cookie)

			params.UserInfo.Apellido = cookie["ln"]
			params.UserInfo.Nombre = cookie["fn"]
			params.UserInfo.UserName = cookie["sub"]
			params.UserInfo.Rol = cookie["rol"]
			params.UserInfo.Grupo = cookie["grp"]
			params.UserInfo.Cliente = cookie["cid"]

			ctx := context.WithValue(r.Context(), "RequestParams", params)
			next.ServeHTTP(w, r.WithContext(ctx))
		})
	}
}

type userInfo struct {
	Nombre   string
	Apellido string
	Rol      string
	Grupo    string
	UserName string
	Cliente  string
}

func RedirectLogin(authServer string) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		ref := r.Host + r.URL.RequestURI()
		log.Printf("%#v", r.RequestURI)

		if r.TLS != nil {
			//				authServer = "https://" + authServer
			ref = "https://" + ref
		} else {
			//				authServer = "http://" + authServer
			ref = "http://" + ref
		}

		c := &http.Cookie{
			Name:     "ri",
			Secure:   false,
			HttpOnly: false,
			Path:     "/",
			Value:    ref,
		}

		http.SetCookie(w, c)
		http.Redirect(w, r, authServer, 307)
	}
}

/*
func RedirectLogin(w http.ResponseWriter, r *http.Request) {
	ref :=  r.Host +  r.URL.RequestURI()
	log.Printf("%#v",r.RequestURI)

	authServer := "test.com:8089/login";
	if r.TLS != nil {
		authServer = "https://" + authServer
		ref = "https://" + ref
	} else {
		authServer = "http://" + authServer
		ref = "http://" + ref
	}

	c := &http.Cookie{
		Name: "ri",
		Secure: false,
		HttpOnly: false,
		Path: "/",
		Value: ref,
	}

	http.SetCookie(w,c)
	http.Redirect(w, r, authServer, 307)
}
*/

//Logout
//	Elimina los tokens de las cookies y los headers
func Logout(w http.ResponseWriter, r *http.Request) {
	c := &http.Cookie{
		Name:     "jwt",
		Secure:   false,
		HttpOnly: false,
		Path:     "/",
		Value:    "***",
		MaxAge:   -1,
		Domain:   os.Getenv("API_TOKEN_DOMAIN"),
	}
	http.SetCookie(w, c)

	//TODO: borrar token de los headers

	app.Success(w, "", "")
}

//JWTCustomerFilter Middleware utilizado para agregar en los filtros, el id de cliente que posee el jwt del request.
func JWTCustomerFilter(errorHandler http.HandlerFunc) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

			params := ContextParams(r)
			params.QueryParams.Del("_idCliente")
			idCliente := params.UserInfo.Cliente
			log.Printf("UserInfo: %#v", params.UserInfo)
			params.QueryParams.Set("_idCliente", idCliente)

			next.ServeHTTP(w, r)

		})
	}
}

//CustomerRestrict Middleware utilizado para reestringir el acceso a los datos
func CustomerRestrict(errorHandler http.HandlerFunc) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

			params := ContextParams(r)
			_idCliente := params.QueryParams.Get("_idCliente")

			log.Printf("Permiso: %#v", params.Permisos)
			log.Printf("ParamsData: %#v", params.RequestBody.Data)
			//Si no tiene IDCliente en el token valido si tiene permiso E1
			if !params.Permisos.Esp1 {

				switch r.Method {

				case "GET":
					if _idCliente == "" || _idCliente == "0" {
						//Si tiene permiso E1 lo dejo pasar
						if !params.Permisos.Esp1 {
							app.Fail(w, http.StatusText(401), 401)
							return

						}

					} else {

						params.QueryParams.Set("idcliente", _idCliente)
					}

				case "POST", "PUT":
					log.Printf("POST")
					if _idCliente == "" || _idCliente == "0" {

						//Si tiene permiso E1 lo dejo pasar
						if !params.Permisos.Esp1 {
							app.Fail(w, http.StatusText(401), 401)
							log.Printf("Sin Permiso ")
							return
						}

					} else {

						if params.RequestBody.Data != nil {
							var objetos []interface{}
							for _, b := range params.RequestBody.Data.([]interface{}) {

								objeto := b.(map[string]interface{})
								objeto["idCliente"], _ = strconv.Atoi(_idCliente)

								objetos = append(objetos, objeto)
							}

							params.RequestBody.Data = objetos
						}
					}

				case "PATCH":
					if _idCliente == "" || _idCliente == "0" {
						//Si tiene permiso E1 lo dejo pasar
						if !params.Permisos.Esp1 {
							app.Fail(w, http.StatusText(401), 401)
							return

						}

					} else {
						var iobjeto interface{}
						objeto := params.RequestBody.Data.(map[string]interface{})
						delete(objeto, "idCliente")
						iobjeto = objeto
						params.RequestBody.Data = iobjeto
						// objeto["idCliente"], _ = strconv.Atoi(_idCliente)

					}

				case "DELETE":
					if _idCliente == "" || _idCliente == "0" {
						//Si tiene permiso E1 lo dejo pasar
						if !params.Permisos.Esp1 {
							app.Fail(w, http.StatusText(401), 401)
							return
						}

					}

				}
			}
			next.ServeHTTP(w, r)
			return
		})
	}
}

//Permisos Estructura para guardar en el contexto los permisos para la peticion actual
type permiso struct {
	Create bool
	Read   bool
	Update bool
	Delete bool
	Esp1   bool
	Esp2   bool
}

func toBool(value uint64) bool {
	if value == 0 {
		return false
	} else {
		return true
	}

}

//Authorization Verifica el perfil y valida si puede acceder al recurso solicitado
func Authorization(errorHandler http.HandlerFunc, recurso string) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			var log = _log.With().Str("ctx", "Authorization").Logger()
			params := ContextParams(r)
			//	token := jwtauth.TokenFromCookie(r)
			reqRol := params.UserInfo.Rol
			reqMethod := r.Method

			log.Trace().Interface("app.Roles.Recursos", app.Roles.Recursos).Str("modulo", app.Module.ModuleName).Send()
			module, ok := app.Roles.Recursos[app.Module.ModuleName]
			if ok == false {
				app.Fail(w, http.StatusText(403), 403)
				return
			}

			permisos := module[recurso]

			Rol, ok := app.Roles.Perfiles[reqRol]

			//Guardo los permisos en el contexto
			params.Permisos.Create = toBool(Rol & permisos.Perfiles_C)
			params.Permisos.Read = toBool(Rol & permisos.Perfiles_R)
			params.Permisos.Update = toBool(Rol & permisos.Perfiles_U)
			params.Permisos.Delete = toBool(Rol & permisos.Perfiles_D)
			params.Permisos.Esp1 = toBool(Rol & permisos.Perfiles_E1)
			params.Permisos.Esp2 = toBool(Rol & permisos.Perfiles_E2)

			ctx := context.WithValue(r.Context(), "RequestParams", params)

			if ok == false {
				log.Printf("Error")
				app.Fail(w, http.StatusText(403), 403)
				return
			}

			// log.Printf("ROL: %v", Rol)

			if (reqMethod == "POST" || reqMethod == "PUT") && (permisos.Perfiles_C != 0 && Rol&permisos.Perfiles_C > 0) {
				// log.Printf("Puede Escribir")
				next.ServeHTTP(w, r.WithContext(ctx))
				return
			}

			if reqMethod == "GET" && permisos.Perfiles_R != 0 && Rol&permisos.Perfiles_R > 0 {

				// log.Printf("Puede Leer")
				next.ServeHTTP(w, r.WithContext(ctx))
				return
			}

			if reqMethod == "PATCH" && permisos.Perfiles_U != 0 && Rol&permisos.Perfiles_U > 0 {
				// log.Printf("Puede Actualizar")
				next.ServeHTTP(w, r.WithContext(ctx))
				return
			}
			if reqMethod == "DELETE" && permisos.Perfiles_D != 0 && Rol&permisos.Perfiles_D > 0 {
				// log.Printf("Puede Actualizar")
				next.ServeHTTP(w, r.WithContext(ctx))
				return
			}

			app.Fail(w, http.StatusText(403), 403)

		})
	}
}
