package middleware

import (
	"fmt"
	"io/ioutil"
	// "log"
	"math/rand"
	"net/http"
	"os"
	"strconv"
	"time"

	// "encoding/json"
	// "context"

	"gitlab.com/mind-framework/core/nz-api/app"
)

//DelayResponse Retrasa el response por un tiempo aleatorio
func DelayResponse() func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

			rand.Seed(time.Now().UnixNano())
			min := 100
			max := 2000
			duration := rand.Intn(max-min) + min
			time.Sleep(time.Duration(duration) * time.Millisecond)
			next.ServeHTTP(w, r)
		})
	}
}

//ResponseOption Middlewear
func ResponseOption() func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

			if r.Method == "HEAD" && r.RequestURI == "/" {
				params := ContextParams(r)
				log.Trace().Str("ctx","ResponseOption").Interface("params",params).Msg("")
				app.Success(w, nil, app.Roles)
			} else {
				next.ServeHTTP(w, r)
			}

		})
	}
}

func ReadCookie() func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

			params := ContextParams(r)

			//fmt.Printf("CTX: %v\n", ctx)
			log.Trace().Str("ctx","ReadCookie").Interface("params",params).Msg("")
			c, err := r.Cookie("jwt")
			if err != nil {
				log.Error().Err(err).Str("ctx","ReadCookie").Msg("")
				app.Fail(w, err.Error())
				return
				//w.Write([]byte("error in reading cookie : " + err.Error() + "\n"))
			} else {
				value := c.Value
				//w.Write([]byte("cookie has : " + value + "\n"))
				fmt.Printf("cookie: %+v\n", value)
				next.ServeHTTP(w, r)
			}
		})
	}
}

//AuditEvents Middleware para recaudar informacion de cada peticion y enviarla por RCP a api.audit
func AuditEvents(ModuleName string, file *os.File) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

			next.ServeHTTP(w, r)

			params := ContextParams(r)

			var ev map[string]string
			ev = make(map[string]string)
			ev["moduleName"] = ModuleName

			if params != nil {

				ev["userName"] = params.UserInfo.UserName
				ev["perfil"] = params.UserInfo.Rol
			}

			ev["resultado"] = strconv.Itoa((*app.Rendered).Code)
			ev["mensaje"] = (*app.Rendered).Message.(string)
			ev["uri"] = r.RequestURI
			ev["accion"] = r.Method
			ev["ipOrigen"] = r.RemoteAddr

			responseData, err := ioutil.ReadAll(r.Body)
			if err != nil {
				log.Error().Err(err).Msg("")
				return
			}

			responseString := string(responseData)
			fmt.Fprint(w, responseString)

			clientRCP, err := app.ListenerRPC.Connect(os.Getenv("API_MODULE_AUDIT") + ":8767")
			if err != nil {
				log.Error().Err(err).Msg("Error al conectar al modulo Audit")
				app.Fail(w, "Error al conectar al modulo Audit", 500)
				return
			}
			defer clientRCP.Close()
			var rp int
			//Si se pudo conectar, intento enviar los eventos
			if err == nil {

				err = clientRCP.Call("Events.SetEvent", ev, &rp)
				if err != nil {
					log.Error().Err(err).Str("ctx","RPC").Interface("event",ev).Msg("ERROR al registrar evento")
				}
			}

			if err != nil {
				t := time.Now()
				fecha := t.Format("2006-01-02")
				hora := t.Format("15:04:05")
				event := fecha + " " + hora + "," + fecha + "," + hora + "," + ModuleName + ","
				event += ev["userName"] + "," + ev["perfil"] + "," + ev["uri"] + "," + ev["resultado"] + "," + ev["accion"] + "," + ev["referer"] + "," + ev["ipOrigen"] + "," + ev["mensaje"] + ";"

				_, err := file.WriteString(event + "\n")
				if err != nil {
					log.Error().Err(err).Msg("Error al escribir archivo")
				}
			}

		})
	}
}

//ConsultaIDUsuario setea en los parametros el id de usuario que realiza la consulta
func ConsultaIDUsuario(errorHandler http.HandlerFunc) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

			params := ContextParams(r)
			userName := params.UserInfo.UserName
			idUsuario := 0

			// params.QueryParams.Del("_idCliente")

			clientRCP, err := app.ListenerRPC.Connect(os.Getenv("API_MODULE_CORE") + ":8767")
			if err != nil {
				app.Fail(w, "Error al conectar al modulo core", 500)
				return
			}
			defer clientRCP.Close()

			clientRCP.Call("Usuarios.ObtenerIDUsuario", userName, &idUsuario)

			// params.QueryParams.Set("_idReferente", strconv.Itoa(referente))
			params.QueryParams.Set("_usuarioActual", strconv.Itoa(idUsuario))

			next.ServeHTTP(w, r)

		})
	}
}
