//Package middleware project middlewares.go
package middleware

import (
	"context"
	// "log"
	"mime/multipart"
	"net/http"

	"gitlab.com/mind-framework/core/nz-api/app"
	"github.com/go-chi/chi"
	// "gitlab.com/mind-framework/core/nz-api/app"

	//	"github.com/mind74/mind-core/pkg/app"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/url"
	"regexp"
	"strconv"
	"strings"
)

//ParseParamsMDW es un middleware que obtiene los parámetros de la solicitud y los almacena en el contexto
func ParseParamsMDW(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		params := ContextParams(r)
		ctx := r.Context()
		//		params := requestParams{}
		var err error

		params.Search = parseSearch(r.URL.Query().Get("q"))

		offset := r.URL.Query().Get("offset")
		if offset == "" {
			offset = r.URL.Query().Get("Offset")
		}
		offset = strings.TrimSpace(offset)

		limit := r.URL.Query().Get("limit")
		if limit == "" {
			limit = r.URL.Query().Get("Limit")
		}
		limit = strings.TrimSpace(limit)

		params.Limit = limitParam{}
		params.Limit.Offset, _ = strconv.ParseInt(offset, 10, 64)
		params.Limit.Count, err = strconv.ParseInt(limit, 10, 64)
		if err != nil {
			params.Limit.Count = -1
		}

		sl := &sortList{}
		err = sl.Parse(r.URL.Query().Get("sort"))

		params.Sort = sl

		//vuelvo a guardar los datos como json
		params.RequestBody.DataJSON, err = json.Marshal(params.RequestBody.Data)
		if err != nil {
			app.Fail(w, err.Error())
			return
		}

		//parseoArchivos
		if r.MultipartForm != nil && r.MultipartForm.File != nil {
			r.ParseMultipartForm(10 << 20)

			fls := r.MultipartForm.File["files"]

			for _, file := range fls {

				f, er := file.Open()
				defer f.Close()

				if er != nil {
					log.Printf("Error al abrir archivo %v", file.Filename)
					continue
				}
				tmpFile := FileData{}
				// tmpFile.Header = file.Header
				tmpFile.Filename = file.Filename
				tmpFile.Size = file.Size
				tmpFile.ContentType = file.Header.Get("Content-Type")
				tmpFile.File = f

				params.Files = append(params.Files, tmpFile)

			}
		}
		//parseo el body
		r.Body = http.MaxBytesReader(w, r.Body, 20971520)
		b, err := ioutil.ReadAll(r.Body)
		if err != nil {
			app.Fail(w, err.Error())
			return

		}

		json.Unmarshal(b, &params.RequestBody.Data)

		//		fmt.Printf("data recibido: %#v\n", string(b))
		//		fmt.Printf("json recibido: %#v\n", params.RequestBody.DataJSON)
		params.QueryParams = r.URL.Query()
		params.Chi = chi.RouteContext(ctx)

		ctx = context.WithValue(r.Context(), "RequestParams", params)

		next.ServeHTTP(w, r.WithContext(ctx))
	}

	return http.HandlerFunc(fn)
}

type sortParam struct {
	Field string
	Order string
}

type sortList struct {
	list []sortParam
}

//parseSort
//	parsea el parámetro sort recibido, armando una lista de los campos por los que
//	se debe ordernar
func (sl *sortList) Parse(param string) error {
	var r []sortParam

	re := regexp.MustCompile("^[\\w]+$")

	sort := strings.Split(param, ",")
	for _, s := range sort {
		//limpio los espacios
		s = strings.TrimSpace(s)

		//por defecto ordeno ascendente
		o := "+"

		//limpio los prefijos
		if strings.HasPrefix(s, "-") {
			s = strings.TrimLeft(s, "-")
			o = "-"
		} else if strings.HasPrefix(s, "+") {
			s = strings.TrimLeft(s, "+")
		}

		//vuelvo a limpiar los espacios
		s = strings.TrimSpace(s)

		//si no es un campo válido, lo ignoro
		if str := re.FindString(s); str == "" {
			continue
		}

		r = append(r, sortParam{s, o})

	}
	sl.list = r
	return nil
}

//Sql Devuelve la representación SQL de la lista de sorts
func (sl *sortList) Sql(dialect string, includeSentece bool) (string, error) {
	var list []string
	var sql string

	switch dialect {
	case "mysql":
		for _, s := range sl.list {
			str := "`" + s.Field + "`"
			if s.Order == "-" {
				str += " DESC"
			}
			list = append(list, str)
		}

		sql = strings.Join(list[:], ", ")
		if includeSentece && sql != "" {
			sql = " ORDER BY " + sql + " "
		}
	case "mssql":
		for _, s := range sl.list {
			str := "[" + s.Field + "]"
			if s.Order == "-" {
				str += " DESC"
			}
			list = append(list, str)
		}

		sql = strings.Join(list[:], ", ")
		if includeSentece && sql != "" {
			sql = " ORDER BY " + sql + " "
		}
	default:
		return sql, errors.New("BD no soportada")
	}

	return sql, nil
}

type limitParam struct {
	Offset int64
	Count  int64
}

func (l *limitParam) Sql(dialect string, includeSentece bool) (string, error) {
	sql := ""
	switch dialect {
	case "mysql":
		if l.Count >= 0 {
			if includeSentece {
				sql += " LIMIT "
			}
			if l.Offset > 0 {
				sql += fmt.Sprintf("%v,%v ", l.Offset, l.Count)
			} else {
				sql += fmt.Sprintf("%v ", l.Count)
			}
		}

	default:
		return sql, errors.New("BD no soportada")
	}
	return sql, nil
}

type searchParam struct {
	Str  string
	Type string
}

//Estructura que contiene los parámetros de la solicitud
type requestParams struct {
	//Contiene la lista de campos para el sort del resultado
	Sort *sortList

	//Contiene el texto para realización de search (parámetro Get q)
	Search *searchParam

	//Contiene los parámetros de offset y limit del request
	Limit limitParam

	//contiene los datos parseados del  body recibido via POST/PUT
	RequestBody requestBody

	//Realiza la búsqueda de un parámetro Get recibido en el request
	QueryParams url.Values

	//Referencia al Contexto del ruteador chi
	Chi *chi.Context

	//Contiene la información del usuario obtenida del token jwt
	UserInfo userInfo

	Permisos permiso

	//Contiene los archivos y la informacion de los mismos del form-data
	Files []FileData
}

type requestBody struct {
	Data     interface{} `json:"data"`
	DataJSON []byte      `json:"-"`
	//	Params 	 interface{} 	`json:"params"`
}

type FileData struct {
	Filename string
	// Header textproto.MIMEHeader
	File        multipart.File
	Size        int64
	ContentType string
	// content  []byte
	// tmpfile  string
}

//parseSearch
//
//  Parsea el parámetro de búsqueda
func parseSearch(param string) (s *searchParam) {
	if param == "" {
		return nil
	}
	s = &searchParam{param, "substr"}

	return
}

//Devuelve los parámetros del request actual
func ContextParams(r *http.Request) *requestParams {
	ctx := r.Context()
	v := ctx.Value("RequestParams")
	if v == nil {
		return &requestParams{}
	}
	return v.(*requestParams)
}
