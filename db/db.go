package db

import (
	"errors"
	"github.com/jinzhu/gorm"
	"gitlab.com/mind-framework/core/nz-api/db/dbconn"
	"strings"
)

//IDB ...
// type IDB interface {
// 	GetInstance(ds *map[string]interface{}) *gorm.DB
// }
var instances map[string]*Instance

func init() {
	instances = make(map[string]*Instance)
}

//Open Establece conexion con base de datos y retorna *gorm.DB
func Open(ds DataSource, name string) (*gorm.DB, error) {
	if v, ok := instances[name]; ok && v.DB != nil {
		return instances[name].DB, nil
	}

	// var err error

	switch strings.ToLower(*ds.Type) {
	// case "mssql":
	// 	con := new(dbconn.MsSQL)
	// 	instances[instanceName].DB = con.Connect(ds)
	case "mysql":
		my := new(dbconn.MySQL)
		my.Config.UserName = *ds.Username
		my.Config.Secret = *ds.Secret
		my.Config.Host = *ds.Host
		my.Config.DBName = *ds.DBName

		i := new(Instance)
		i.DB = my.Connect()
		my.Migrate(i.DB, name)
		i.DataSource = ds
		instances[name] = i

	default:
		return nil, errors.New("Tipo de base de datos no soportada")
	}

	return instances[name].DB, nil
}

//GetInstance Retonra una instancia inicializada
func GetInstance(ds *DataSource, name string) *gorm.DB {
	if v, ok := instances[name]; ok && v.DB != nil {
		return instances[name].DB

	}
	Open(*ds, name)
	return instances[name].DB
}
