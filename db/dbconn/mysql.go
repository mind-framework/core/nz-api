package dbconn

import (

	// "github.com/go-sql-driver/mysql"
	// "database/sql"

	"github.com/jinzhu/gorm"

	"time"

	"io/ioutil"

	"github.com/golang-migrate/migrate"
	"github.com/golang-migrate/migrate/database/mysql"
	_ "github.com/golang-migrate/migrate/source/file"
	"github.com/rs/zerolog/log"
	"gitlab.com/mind-framework/core/nz-api/app"
)

// MySQL Establece conexion con bases de datos Microsoft SQL Server
type MySQL struct {
	Config struct {
		UserName string
		Secret   string
		Host     string
		DBName   string
	}

	// db *gorm.DB
}

// Connect Metodo para conectar a la instancia de base de datos
func (m *MySQL) Connect() *gorm.DB {

	var db *gorm.DB
	var err error
	for {

		db, err = gorm.Open("mysql", m.Config.UserName+":"+m.Config.Secret+"@tcp("+m.Config.Host+")/"+m.Config.DBName+
			"?parseTime=true&multiStatements=true")
		if err != nil {
			// log.Println("error al conectar a BD '" + *dbHost + "(" + err.Error() + ")" + "', reintentando")
			log.Error().Str("dbHost", m.Config.Host).Err(err).Msg("error al conectar a BD")
			time.Sleep(5 * time.Second)
			continue
		}

		// defer db.Close()
		// m.db=db

		break

	}
	return db
}

// Migrate Ejecuta scripts SQL
func (m *MySQL) Migrate(db *gorm.DB, folder string) error {

	log.Info().Msg("Iniciando Migración")
	files, err := ioutil.ReadDir("migrate/" + folder)
	if err != nil {
		log.Warn().Msgf("Carpeta de migración sql '%v' no encontrada", "migrate/"+folder)
		return nil
	}
	if len(files) == 0 {
		log.Info().Msgf("No hay archivos para migrar")
		return nil
	}

	driver, _ := mysql.WithInstance(db.DB(), &mysql.Config{MigrationsTable: "schema_migrations_" + app.Module.ModuleName})

	mig, err := migrate.NewWithDatabaseInstance(
		"file://migrate/"+folder,
		"mysql",
		driver,
	)

	if err != nil {
		log.Fatal().Err(err).Str("cat", "migrate").Msg("no se pudo realizar migración")
		return err
	}

	if err := mig.Up(); err != nil && err.Error() != "no change" {
		log.Fatal().Err(err).Str("cat", "migrate").Msg("Error en Up")
		return err
	} else if err == nil {
		log.Info().Msgf("Migracion de DB %v completada", folder)
	}

	return nil
}

//GetInstance Retorna una instancia inicializada, si no existe, la crea
// func (m *MySQL) GetInstance(ds *map[string]interface{}) *gorm.DB {

// 	if m.db == nil {
// 		m.connect(ds)
// 	}

// 	return m.db

// }
