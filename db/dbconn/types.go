package dbconn

import (
	"github.com/jinzhu/gorm"
)

//DataSource Estructura de datos requeridos para establecer conexion a base de datos
type DataSource struct {

	//Host IP o Server/Instance
	Host     *string
	Username *string
	Secret   *string
	DBName   *string
	Port     *string
	Type     *string
}

//Instance Estructura con los datos de conexion, y el puntero a una instancia
type Instance struct {
	DB *gorm.DB
	DataSource
}
