package dbconn


import (
	
	// "github.com/jinzhu/gorm/dialects/mssql"
	"github.com/jinzhu/gorm"

	"github.com/rs/zerolog/log"
	"time"
)
//MsSQL Establece conexion con bases de datos Microsoft SQL Server
type MsSQL struct{
	db *gorm.DB
}

//connect Metodo para conectar a la instancia de base de datos
func (m *MsSQL)connect(ds *map[string]interface{}) {

	for {
			
			db, err := gorm.Open("mssql", "sqlserver://"+(*ds)["Username"].(string)+":"+(*ds)["Secret"].(string)+"@"+(*ds)["Host"].(string)+"?database="+(*ds)["DBName"].(string))
			if err != nil {
				// log.Println("error al conectar a BD '" + *dbHost + "(" + err.Error() + ")" + "', reintentando")
				log.Error().Str("dbHost", (*ds)["Host"].(string)).Err(err).Msg("error al conectar a BD")
				time.Sleep(5 * time.Second)
				continue
			}

			// defer db.Close()
			m.db=db


			break
	
	}
}

//GetInstance Retorna una instancia inicializada, si no existe, la crea
func (m *MsSQL)GetInstance(ds *map[string]interface{}) *gorm.DB {

	if m.db == nil {
		m.connect(ds)
	}
	
	return m.db

}






