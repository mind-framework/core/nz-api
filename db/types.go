package db

import (
	"github.com/jinzhu/gorm"
)

//DataSource Estructura de datos requeridos para establecer conexion a base de datos
type DataSource struct {

	//Host IP o Server/Instance
	Host     *string
	Username *string
	Secret   *string
	DBName   *string
	Port     *string
	Type     *string
}

//Instance Estructura con los datos de conexion, y el puntero a una instancia
type Instance struct {
	DB         *gorm.DB
	DataSource DataSource
}

// //IMigrate Interface para tipos de bases de datos que soporten Migracion
// type IDB interface {
// 	GetHostName() string
// 	GetUserName() string
// 	GetSecret() string

// 	Migrate(*gorm.DB, string) error
// }
